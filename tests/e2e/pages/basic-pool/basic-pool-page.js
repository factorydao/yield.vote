/* eslint-disable ui-testing/no-hard-wait */
/* eslint-disable cypress/no-unnecessary-waiting */
import Page from '../page';

export default class BasicPoolPage extends Page {
	constructor() {
		super();
	}

	visit() {
		cy.visit('/#/pool/most-basic');
		cy.wait(5000);
	}

	getDepositButton() {
		return cy.get('.pool-info__deposit .btn-secondary');
	}

	getMaxDepositButton() {
		return cy.get('.pool-info__deposit .btn-outline');
	}

	getDeposit() {
		return cy.get('.pool-info__deposit-input-container input');
	}

	setDeposit(val) {
		this.getDeposit().type(val);
	}

	getOpenPositionsCounter() {
		return cy.get('.occupied-lots__title .btn strong');
	}

	getFirstOpenPositionWithdrawButton() {
		return cy.get('.occupied-lots__table-container .btn').first();
	}
}