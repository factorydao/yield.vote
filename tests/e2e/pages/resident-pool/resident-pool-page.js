/* eslint-disable ui-testing/no-hard-wait */
/* eslint-disable cypress/no-unnecessary-waiting */
import Page from '../page';

export default class ResidentPoolPage extends Page {
	constructor() {
		super();
	}

	visit() {
		cy.visit('/#/pool/resident/1');
		cy.wait(15000);
	}

	getStakeBtn() {
		return cy.get('.pool-info__actions .btn-secondary');
	}

	getWithdrawBtn() {
		return cy.get('.pool-info__actions .btn-outline-secondary');
	}

	getMyLotsCounter() {
		return cy.get('.occupied-lots__title .nav-item strong')
	}

	getStakeFormTitle() {
		return cy.get('.resident-stake-lp__lot-title')
	}

	getStakeFormDepositInput() {
		return cy.get('.resident-stake-lp__card .deposit')
	}

	getStakeFormBurnRateInput() {
		return cy.get('.resident-stake-lp__card .burn-rate')
	}

	getStakeFormClaimBtn() {
		return cy.get('.resident-stake-lp__card .btn')
	}

	getVacateBtn() {
		return cy.get('.resident-withdraw__card .btn-outline-secondary')
	}
}