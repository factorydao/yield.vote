/* eslint-disable ui-testing/no-hard-wait */
/* eslint-disable cypress/no-unnecessary-waiting */
import Page from '../page';

export default class HomePage extends Page {
	constructor() {
		super();
	}

	visit() {
		cy.visit('/');
		cy.wait(5000);
	}

	getHeaderAddress() {
		return cy.get('.app-header__menu strong');
	}

	getPoolBasicWidgetStartDate() {
		return cy.get('.pool-basic-widget__start-date');
	}

	getPoolBasicWidgetButton() {
		return cy.get('.pool-basic-widget .btn');
	}

	getPoolResidentWidgetFilledLots() {
		return cy.get('.pool-resident-widget__filled-lots');
	}

	getPoolResidentWidgetButton() {
		return cy.get('.pool-resident-widget .btn');
	}

	getPoolTycoonWidgetButton() {
		return cy.get('.pool-tycoon-widget .btn');
	}
}