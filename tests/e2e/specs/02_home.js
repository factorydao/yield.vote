import HomePage from '../pages/home/home-page';
import { formatAddress } from '../../../src/utils/formats'

const home = new HomePage();

let metamaskWalletAddress;

describe('Home page tests', () => {
    before(() => {
		home.visit();
		home.getDisclaimerFormCheckbox().click();
		home.getDisclaimerFormButton().click();
		home.getMetamaskWalletAddress().then((address) => {
			metamaskWalletAddress = address;
		});
	});
	context('Initial', () => {
		it(`should display current account address in header`, () => {
            home.getHeaderAddress().should('have.text', formatAddress(metamaskWalletAddress).toLowerCase());
		});
		it(`basic pool widget should show data`, () => {
			home.getPoolBasicWidgetStartDate().should('not.have.text', '');
		});
		it(`basic pool widget button should be enabled and navigate to basic pool page`, () => {
            home.getPoolBasicWidgetButton().should('not.have.attr', 'disabled');
			home.getPoolBasicWidgetButton().click();
			cy.wait(5000);
			home.getH1().should('have.text', 'Basic Pool');
			home.visit();
			home.getDisclaimerFormCheckbox().click();
			home.getDisclaimerFormButton().click();
		});
		it(`resident pool widget should show data`, () => {
			home.getPoolResidentWidgetFilledLots().should('not.have.text', '');
		});
		it(`resident pool widget button should be enabled and navigate to resident pool page`, () => {
            home.getPoolResidentWidgetButton().should('not.have.attr', 'disabled');
			home.getPoolResidentWidgetButton().click();
			cy.wait(5000);
			home.getH1().should('have.text', 'Resident Pool');
			home.visit();
			home.getDisclaimerFormCheckbox().click();
			home.getDisclaimerFormButton().click();
		});
		it(`tycoon pool widget button should be disabled`, () => {
            home.getPoolTycoonWidgetButton().should('have.attr', 'disabled');
		});
	});
});