import ResidentPoolPage from '../pages/resident-pool/resident-pool-page';

const residentPool = new ResidentPoolPage();

describe('Resident pool tests', () => {
    before(() => {
		residentPool.visit();
		residentPool.getDisclaimerFormCheckbox().click();
		residentPool.getDisclaimerFormButton().click();
	});
	context('Initial', () => {
		it(`withdraw button should be disabled`, () => {
			residentPool.getWithdrawBtn().should('have.attr', 'disabled');
		});
		it(`should stake lot and vacate`, () => {
			residentPool.getMyLotsCounter().should('have.text', '0');
            residentPool.getStakeBtn().click();
			cy.wait(1500);
			if(residentPool.getStakeFormTitle().invoke('text') == 'Lot 1 - Vacant') {
				residentPool.getStakeFormDepositInput().type('28');
				residentPool.getStakeFormBurnRateInput().type('0.2');
				residentPool.getStakeFormClaimBtn().click();
				cy.wait(5000);
				residentPool.confirmMetamaskTransaction();
				cy.wait(90000);
				residentPool.getWithdrawBtn().should('not.have.attr', 'disabled');
				residentPool.getMyLotsCounter().should('have.text', '1');
				residentPool.getWithdrawBtn().click();
				cy.wait(1500);
				residentPool.getVacateBtn().click();
				residentPool.confirmMetamaskTransaction();
				cy.wait(90000);
				residentPool.getMyLotsCounter().should('have.text', '0');
			} else {
				residentPool.getStakeFormDepositInput().type('280');
				residentPool.getStakeFormBurnRateInput().type('0.5');
				residentPool.getStakeFormClaimBtn().click();
				cy.wait(5000);
				residentPool.confirmMetamaskTransaction();
				cy.wait(90000);
				residentPool.getWithdrawBtn().should('not.have.attr', 'disabled');
				residentPool.getMyLotsCounter().should('have.text', '1');
				residentPool.getWithdrawBtn().click();
				cy.wait(1500);
				residentPool.getVacateBtn().click();
				residentPool.confirmMetamaskTransaction();
				cy.wait(90000);
			}
		});
	});
});