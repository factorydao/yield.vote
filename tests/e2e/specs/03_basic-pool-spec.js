import BasicPoolPage from '../pages/basic-pool/basic-pool-page';

const basicPool = new BasicPoolPage();

describe('Basic pool tests', () => {
    before(() => {
		basicPool.visit();
		basicPool.getDisclaimerFormCheckbox().click();
		basicPool.getDisclaimerFormButton().click();
	});
	context('Initial', () => {
		it(`deposit button should be disabled without value`, () => {
            basicPool.getDepositButton().should('have.attr', 'disabled');
		});
		it(`max button should set value`, () => {
            basicPool.getMaxDepositButton().click();
			basicPool.getDeposit().should('not.have.value', '');
		});
		it(`deposit button should be enabled with value`, () => {
			basicPool.setDeposit(1);
            basicPool.getDepositButton().should('not.have.attr', 'disabled');
		});
		it(`should make deposit and withdraw`, () => {
			basicPool.getOpenPositionsCounter().should('have.text', '0');
			basicPool.setDeposit(1);
            basicPool.getDepositButton().click();
			basicPool.confirmMetamaskTransaction();
			cy.wait(15000);
			basicPool.getOpenPositionsCounter().should('have.text', '1');
			basicPool.getFirstOpenPositionWithdrawButton().click();
			basicPool.confirmMetamaskTransaction();
			cy.wait(15000);
			basicPool.getOpenPositionsCounter().should('have.text', '0');
		});
	});
});