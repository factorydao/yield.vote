#!/usr/bin/env bash

scp -r ./build/ ipfs-node-fv:~/launchpad/yield
ssh ipfs-node-fv "./add_ipfs.sh yield;exit;"
scp -r ./build/ dolphin:~/launchpad/yield
ssh dolphin "./add_ipfs.sh yield;exit;"
