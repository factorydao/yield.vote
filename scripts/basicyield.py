import json
from web3 import Web3
import os
from dotenv import load_dotenv
import pandas as pd

load_dotenv(dotenv_path='.env', verbose=True)
INFURA_API_KEY = os.getenv("INFURA_API_KEY")

# w3 = Web3(Web3.WebsocketProvider('wss://ropsten.infura.io/ws/v3/{}'.format(INFURA_API_KEY)))
w3 = Web3(Web3.WebsocketProvider('wss://mainnet.infura.io/ws/v3/{}'.format(INFURA_API_KEY)))
print('Current working directory: {}'.format(os.getcwd()))
print('INFURA_API_KEY: {}'.format(INFURA_API_KEY))
yieldContractAddress = '0x38A1f049b61024316969a1559F4F093391b1dEF6'


def getYieldData2():
    print('Getting yield data')
    yieldJson = json.load(open('./src/truffle/build/contracts/MostBasicYield.json', 'r'))
    yieldContract = w3.eth.contract(address=yieldContractAddress, abi=yieldJson['abi'])
    depositFilter = yieldContract.events.DepositOccurred.createFilter(fromBlock="0x0")
    depositEvents = depositFilter.get_all_entries()
    processedDepositEvents = [{**x, **x['args']} for x in depositEvents]
    d = []
    for event in processedDepositEvents:
        block = w3.eth.getBlock(event['blockNumber'])
        rcpt = w3.eth.getTransactionReceipt(event['transactionHash'])
        tx = w3.eth.getTransaction(event['transactionHash'])
        receipt = yieldContract.functions.receipts(event['id']).call()
        d.append({
            'id': event['id'],
            'owner': event['owner'],
            'depositTime': block.timestamp,
            'depositAmount': receipt[1],
            'gasUsed': rcpt['gasUsed'],
            'gasPrice': tx['gasPrice'],
            'blockNumber': block.number
        })
    df = pd.DataFrame(d)
    return df


def getYieldData():
    print('Getting yield data')
    yieldJson = json.load(open('./src/truffle/build/contracts/MostBasicYield.json', 'r'))
    yieldContract = w3.eth.contract(address=yieldContractAddress, abi=yieldJson['abi'])
    depositFilter = yieldContract.events.DepositOccurred.createFilter(fromBlock="0x0")
    depositEvents = depositFilter.get_all_entries()
    processedDepositEvents = [{**x, **x['args']} for x in depositEvents]
    d = {}
    for event in processedDepositEvents:
        block = w3.eth.getBlock(event['blockNumber'])
        receipt = yieldContract.functions.receipts(event['id']).call()
        d[event['id']] = {
            'id': event['id'],
            'owner': event['owner'],
            'depositTime': block.timestamp,
            'depositAmount': receipt[1],
        }

    withdrawalFilter = yieldContract.events.WithdrawalOccurred.createFilter(fromBlock="0x0")
    withdrawalEvents = withdrawalFilter.get_all_entries()
    processedWithdrawalEvents = [{**x, **x['args']} for x in withdrawalEvents]
    for event in processedWithdrawalEvents:
        block = w3.eth.getBlock(event['blockNumber'])
        d[event['id']]['withdrawalTime'] = block.timestamp
    df = pd.DataFrame(d.values())
    df['timeStaked'] = df['withdrawalTime'] - df['depositTime']
    df['rewards'] = df['timeStaked'] * df['depositAmount']
    g = df[['owner', 'rewards']].groupby('owner').sum().sort_values('rewards')
    return g

yieldData = getYieldData2()
print(yieldData)