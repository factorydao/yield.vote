import { ethInstance, fromWei } from "evm-chain-scripts";
import { useQuery } from "react-query";
import { getDepositToken, getWriteContractByType } from "utils/helper";
import { BASIC_CONTRACT_TYPE } from "utils/poolHelper";
import { getResidentAccountAllowance } from "utils/residentPool";

const getCommonPoolsAllowance = async (poolIndex, contractType) => {
  let result = 0;
  if (poolIndex && !poolIndex.startsWith("0x")) {
    const [yieldContract, tokenContract, account] = await Promise.all([
      getWriteContractByType(contractType),
      getDepositToken(contractType, poolIndex),
      ethInstance.getEthAccount(false)
    ]);
    try {
      const currentApproval = await tokenContract.allowance(account, yieldContract.address);
      result = parseFloat(fromWei(currentApproval));
    } catch (error) {
      console.log(error);
    }
    return result;
  }
  return result;
};

const POOL_ALLOWANCE = {
  [BASIC_CONTRACT_TYPE.BASIC]: getCommonPoolsAllowance,
  [BASIC_CONTRACT_TYPE.PERMISSIONLESS]: getCommonPoolsAllowance,
  resident: getResidentAccountAllowance
};

export const usePoolAllowance = (contractType, poolIndexOrTokenAddress) =>
  useQuery(
    ["pool-allowance", contractType, poolIndexOrTokenAddress],
    async () => POOL_ALLOWANCE[contractType](poolIndexOrTokenAddress, contractType),
    { enabled: !!poolIndexOrTokenAddress }
  );
