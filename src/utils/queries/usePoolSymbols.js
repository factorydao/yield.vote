import { useQuery } from "react-query";
import { getAllBasicTokensSymbols } from "utils/basicPool";
import { getAllPermissionlessBasicTokensSymbols } from "utils/permissionlessBasicPool";
import { BASIC_CONTRACT_TYPE } from "utils/poolHelper";
import { getResidentSymbols } from "utils/residentPool";

const POOL_SYMBOLS = {
  [BASIC_CONTRACT_TYPE.BASIC]: getAllBasicTokensSymbols,
  [BASIC_CONTRACT_TYPE.PERMISSIONLESS]: getAllPermissionlessBasicTokensSymbols,
  resident: getResidentSymbols
};

export const usePoolSymbols = (contractType, poolIndex, currentNetwork) =>
  useQuery(
    ["pool-symbols", contractType, poolIndex, currentNetwork],
    async () => POOL_SYMBOLS[contractType](poolIndex),
    {
      enabled: !!(poolIndex || currentNetwork)
    }
  );
