import { useQuery } from "react-query";
import { getBasicGlobalsAll } from "utils/basicPool";
import { getPermissionlessBasicGlobalsAll } from "utils/permissionlessBasicPool";
import { getResidentGlobalsAll } from "utils/residentPool";

const getAllPoolsType = {
  basic: getBasicGlobalsAll,
  permissionlessbasic: getPermissionlessBasicGlobalsAll,
  resident: getResidentGlobalsAll
};

export const useGetPoolsAll = (key) =>
  useQuery(["pools-all", key], async () => getAllPoolsType[key]());
