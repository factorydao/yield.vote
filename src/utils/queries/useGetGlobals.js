import { useQuery } from "react-query";
import { getBasicGlobals } from "utils/basicPool";
import { getPermissionlessBasicGlobals } from "utils/permissionlessBasicPool";
import { BASIC_CONTRACT_TYPE } from "utils/poolHelper";
import { getResidentGlobals } from "utils/residentPool";

export const useResidentPool = (poolIndex, currentNetwork) =>
  useQuery(
    ["getResidentPool", poolIndex, currentNetwork],
    async () => getResidentGlobals(poolIndex, currentNetwork),
    {
      enabled: !!(poolIndex && currentNetwork),
      refetchOnWindowFocus: false
    }
  );

const GLOBALS_TYPES = {
  resident: getResidentGlobals,
  [BASIC_CONTRACT_TYPE.BASIC]: getBasicGlobals,
  [BASIC_CONTRACT_TYPE.PERMISSIONLESS]: getPermissionlessBasicGlobals
};

export const useGetPoolGlobals = (contractType, poolIndex, currentNetwork) =>
  useQuery(
    ["globals", contractType, poolIndex, currentNetwork],
    async () => GLOBALS_TYPES[contractType](poolIndex),
    {
      enabled: !!(poolIndex && currentNetwork),
      refetchOnWindowFocus: false
    }
  );
