import { ethInstance } from "evm-chain-scripts";
import { useQuery } from "react-query";
import { getBasicFromTime, getBasicMaturityRewards } from "utils/basicPool";
import { getPoolInstance } from "utils/helper";
import {
  getPermissionlessBasicFromTime,
  getPermissionlessBasicMaturityRewards
} from "utils/permissionlessBasicPool";
import { BASIC_CONTRACT_TYPE, getPositionsV2 } from "utils/poolHelper";

const GET_FROM_TIME = {
  [BASIC_CONTRACT_TYPE.BASIC]: getBasicFromTime,
  [BASIC_CONTRACT_TYPE.PERMISSIONLESS]: getPermissionlessBasicFromTime
};

const MATURITY_REWARDS = {
  [BASIC_CONTRACT_TYPE.BASIC]: getBasicMaturityRewards,
  [BASIC_CONTRACT_TYPE.PERMISSIONLESS]: getPermissionlessBasicMaturityRewards
};

const getPositionsFromCache = async (contractType, poolIndex, eventName) => {
  const poolInstance = getPoolInstance(contractType);

  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const [poolData, rewardData] = await Promise.all([
    contract.pools(poolIndex),
    contract.getRewardData(poolIndex)
  ]);

  const positions = await getPositionsV2(
    { ...poolInstance, poolData, rewardData, value: poolIndex },
    eventName,
    GET_FROM_TIME[contractType],
    MATURITY_REWARDS[contractType]
  );

  return positions;
};

export const useGetPositions = (contractType, poolIndex, chainId, eventName) =>
  useQuery(
    ["positions", contractType, poolIndex, chainId, eventName],
    async () => getPositionsFromCache(contractType, poolIndex, eventName),
    {
      enabled: !!(poolIndex && chainId && eventName),
      initialData: { activePositions: {}, inActivePositions: {} }
    }
  );
