import { useQuery } from "react-query";
import { getBasicPoolMaxNumber } from "./basicPool";
import { getCurrentBlockNumber } from "./poolHelper";
import {
  getResidentAverageStats,
  getResidentDistanceToPulseEnd,
  getResidentSlot,
  getResidentSlots,
  getResidentWaveLength
} from "./residentPool";

export const useBasicPoolsMaxNumber = () =>
  useQuery("basic-max-pools", async () => getBasicPoolMaxNumber());

// resident - liquidity factory
export const useResidentSlot = (poolIndex, slotId) =>
  useQuery(["getSlot", poolIndex, slotId], async () => getResidentSlot(poolIndex, slotId), {
    enabled: !!(poolIndex && slotId),
    refetchOnWindowFocus: false
  });

export const useResidentSlots = (poolIndex, currentNetwork) =>
  useQuery(
    ["slots", poolIndex, currentNetwork],
    async () => getResidentSlots(poolIndex, currentNetwork),
    {
      refetchInterval: 30000,
      enabled: !!(poolIndex && currentNetwork),
      refetchOnWindowFocus: false,
      initialData: {
        all: [],
        occupied: []
      }
    }
  );

export const useCurrentBlock = () =>
  useQuery("getCurrentBlock", async () => getCurrentBlockNumber());

export const useResidentDistanceToPulseEnd = (poolIndex) =>
  useQuery(["pulseEnd", poolIndex], async () => getResidentDistanceToPulseEnd(poolIndex), {
    refetchInterval: 15000
  });

export const useResidentWavelength = (poolIndex, chainId) =>
  useQuery(
    ["wavelength", poolIndex, chainId],
    async () => getResidentWaveLength(poolIndex, chainId),
    {
      enabled: !!(poolIndex && chainId),
      refetchOnWindowFocus: false,
      staleTime: Infinity
    }
  );
export const useResidentPercentageStats = (slotIds, currentNetwork) =>
  useQuery(
    ["resident", "percentage", "stats", slotIds, currentNetwork],
    async () => getResidentAverageStats(slotIds),
    {
      initialData: {
        avgAPEPY: "",
        avgAPY: ""
      },
      enabled: !!(slotIds && currentNetwork)
    }
  );
