import {
  bytesToString,
  ethInstance,
  fromWei,
  getInterface,
  stringToBytes,
  toBN,
  toWei
} from "evm-chain-scripts";
import memoize from "lodash.memoize";
import specialPools from "specialPools.json";
import { ZERO_ADDRESS } from "./helper";
import {
  accountConnected,
  contracts,
  getBytes,
  getCurrentBlockNumber,
  getTokenSymbolMemoized,
  parseBNData,
  RESULT_TYPES,
  secondsPerBlock,
  secondsPerDay,
  secondsPerYear,
  TRANSACTION_STATUS
} from "./poolHelper";

const poolInstance = {
  contractType: "permissionless", // RESIDENT_CONTRACT_TYPE.PERMISSIONLESS,
  contract: contracts.PermissionlessLiquidityFactory,
  chainId: ethInstance.getChainId()
};

async function getPermissionlessResidentWriteContract() {
  const chainId = ethInstance.getChainId();
  return await ethInstance.getWriteContractByAddress(
    poolInstance.contract,
    poolInstance.contract.networks[chainId].address
  );
}

export async function getPermissionlessResidentPoolMaxNumber() {
  const liquidity = await getPermissionlessResidentWriteContract();
  return await liquidity.numPools();
}

export async function approvePermissionlessResidentSlot(deposit, tokenAddress) {
  const [liquidity, token, account] = await Promise.all([
    getPermissionlessResidentWriteContract(),
    ethInstance.getWriteContractByAddress(contracts.LPToken, tokenAddress),
    ethInstance.getEthAccount()
  ]);

  const currentApproval = parseFloat(fromWei(await token.allowance(account, liquidity.address)));
  if (currentApproval < parseFloat(deposit) || currentApproval === 0) {
    const approvalAmount = process.env.REACT_APP_RESIDENT_POOL_APPROVAL_AMOUNT ?? "1000000";
    return await token.approve(liquidity.address, toWei(approvalAmount));
  }
}

export async function claimPermissionlessResidentSlot(poolIndex, slotId, burnRate, deposit) {
  const liquidity = await getPermissionlessResidentWriteContract();

  const pulses = await liquidity.pulses(poolIndex);
  const wavelength = pulses.pulseWavelengthBlocks;
  const burnPerBlock = toBN(toWei(burnRate.toString())).div(wavelength).toString();
  const depositWei = toWei(deposit.toString());

  return await liquidity.claimSlot(poolIndex, slotId, burnPerBlock, depositWei);
}

export async function updatePermissionlessResidentSlot(poolIndex, slotId) {
  const liquidity = await getPermissionlessResidentWriteContract();
  return await liquidity.updateSlot(poolIndex, slotId);
}

export async function withdrawPermissionlessResidentFromSlot(poolIndex, slotId) {
  const contract = await getPermissionlessResidentWriteContract();
  return await contract.withdrawFromSlot(poolIndex, slotId);
}

export async function getPermissionlessResidentDeposit(poolIndex, slotId) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const slot = await contract.getSlot(poolIndex, slotId);
  const deposit = fromWei(slot[1]);

  return deposit;
}

export async function getPermissionlessResidentRewards(poolIndex, slotId) {
  let result = { rewards1: 0, rewards2: 0 };

  try {
    const contract = await ethInstance.getContract("read", poolInstance.contract);
    const rewards = await contract.getRewards(poolIndex, slotId);
    result = { rewards1: fromWei(rewards[0]), rewards2: fromWei(rewards[1]) };

    return result;
  } catch (error) {
    console.error("getRewards: ", error);
    return result;
  }
}

export async function getMaxPermissionlessResidentStakers(poolIndex) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const pool = await contract.pools(poolIndex);
  return pool.maxStakers;
}

const getMaxPermissionlessResidentStakersMemoized = memoize(getMaxPermissionlessResidentStakers);

export async function getMinPermissionlessResidentDeposit(poolIndex) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);

  return fromWei(await contract.minimumDeposit(poolIndex));
}

export async function getPermissionlessResidentOwner(poolIndex, slotId) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const slot = await contract.getSlot(poolIndex, slotId);

  return slot.owner;
}

export async function getPermissionlessResidentWaveLength(poolIndex) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const pulses = await contract.pulses(poolIndex);
  const wavelength = toBN(pulses.pulseWavelengthBlocks);

  return wavelength;
}

const getWavelengthMemoized = memoize(getPermissionlessResidentWaveLength);

export async function slotIsOccupied(poolIndex, slotId) {
  const owner = await getPermissionlessResidentOwner(poolIndex, slotId);

  return owner === ZERO_ADDRESS;
}

export async function getPermissionlessResidentBurnRate(poolIndex, slotId) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const slot = await contract.slots(poolIndex, slotId);
  return slot.burnRate;
}

export async function getPermissionlessResidentGlobals(poolIndex, currentNetwork) {
  const currentBlock = await (await ethInstance.getBlock("latest", currentNetwork)).number;
  const multi = await getPermissionlessResidentGlobalsMulti(poolIndex);
  const pulse = multi[0];
  const pool = multi[1];
  const poolStats = multi[2];
  const poolMetadata = multi[3];
  const waveLength = pulse.pulseWavelengthBlocks;

  const currentReward = calcResidentRewardForBlock(pulse, toBN(currentBlock));
  const rewardsPerPulse = calcResidentRewardPerPulse(pulse);

  const minBurnRatePerPulse = calcResidentBurnPerPulse(waveLength, pool.minimumBurnRateWeiPerBlock);
  const maxBurnRatePerPulse = calcResidentBurnPerPulse(waveLength, pool.maximumBurnRateWeiPerBlock);
  const poolStartsIn = pulse.pulseStartBlock - currentBlock;
  const poolName = `${bytesToString(poolMetadata.name)}${poolStats.paused ? " (Paused)" : ""}${
    poolStartsIn > 0 ? " (Starts in " + poolStartsIn + " blocks)" : ""
  }`;

  const days = 7;
  const blocksPerDay = Math.floor(secondsPerDay / secondsPerBlock);
  const blocksPerWeek = blocksPerDay * days;
  const pulseLengthByWeeks = pulse.pulseWavelengthBlocks / blocksPerWeek;
  const totalLiquidity = await calcResidentTotalLiqudity(pool);

  const [depositTokenSymbol, rewardTokenSymbol] = await Promise.all([
    getTokenSymbolMemoized(pool.liquidityToken),
    getTokenSymbolMemoized(pulse.rewardToken)
  ]);

  const symbols = {
    depositTokenSymbol: depositTokenSymbol,
    reward1TokenSymbol: rewardTokenSymbol,
    reward2TokenSymbol: rewardTokenSymbol
  };

  return {
    maxStakers: pool.maxStakers,
    minDeposit: fromWei(pool.minimumDepositWei),
    maxDeposit: fromWei(pool.maximumDepositWei),
    minBurnRate: pool.minimumBurnRateWeiPerBlock,
    minBurnRatePerPulse: fromWei(minBurnRatePerPulse),
    maxBurnRatePerPulse: fromWei(maxBurnRatePerPulse),
    pulseStartBlock: pulse.pulseStartBlock,
    pulseWavelength: pulse.pulseWavelengthBlocks,
    pulseAmplitudeWei: pulse.pulseAmplitudeWei,
    pulseLengthByWeeks: pulseLengthByWeeks,
    currentReward: parseFloat(currentReward),
    rewardsPerPulse: parseFloat(fromWei(rewardsPerPulse.toString())),
    totalStaked: fromWei(poolStats.totalStakedWei),
    totalRewards: parseFloat(fromWei(poolStats.totalRewardsWei)),
    totalLiquidity: totalLiquidity,
    name: poolName,
    liquidityToken: pool.liquidityToken,
    paused: poolStats.paused,
    pulseIntegral: fromWei(pulse.pulseIntegral),
    symbols: symbols
  };
}

export async function getPermissionlessResidentGlobalsAll(light = false) {
  try {
    const contract = await ethInstance.getContract("read", poolInstance.contract);
    const poolIndexMax = await contract.numPools();
    const chainId = ethInstance.getChainId();
    const items = [];
    const currentBlock = await getCurrentBlockNumber();

    for (let index = 1; index <= poolIndexMax; index++) {
      if (specialPools.ignoredResident[chainId]?.includes(index)) continue;

      const multi = await getPermissionlessResidentGlobalsMulti(index);
      const pulse = multi[0];
      const pool = multi[1];
      const poolStats = multi[2];
      const poolMetadata = multi[3];

      const poolStartsIn = pulse.pulseStartBlock - currentBlock;
      const poolName = `${bytesToString(poolMetadata.name)}${poolStats.paused ? " (Paused)" : ""}${
        poolStartsIn > 0 ? " (Starts in " + poolStartsIn + " blocks)" : ""
      }`;

      if (light) {
        items.push({
          index: index,
          liquidityToken: pool.liquidityToken,
          name: poolName,
          paused: poolStats.paused
        });
      } else {
        const currentReward = calcResidentRewardForBlock(pulse, toBN(currentBlock));
        const rewardsPerPulse = calcResidentRewardPerPulse(pulse);
        const totalLiquidity = await calcResidentTotalLiqudity(pool);

        items.push({
          index: index,
          maxStakers: pool.maxStakers,
          minDeposit: fromWei(pool.minimumDepositWei),
          maxDeposit: fromWei(pool.maximumDepositWei),
          minBurnRate: pool.minimumBurnRateWeiPerBlock,
          pulseStartBlock: pulse.pulseStartBlock,
          pulseWavelength: pulse.pulseWavelengthBlocks,
          currentReward: parseFloat(currentReward),
          rewardsPerPulse: parseFloat(fromWei(rewardsPerPulse.toString())),
          totalStaked: fromWei(poolStats.totalStakedWei),
          totalRewards: parseFloat(fromWei(poolStats.totalRewardsWei)),
          totalLiquidity: totalLiquidity,
          liquidityToken: pool.liquidityToken,
          name: poolName,
          paused: poolStats.paused
        });
      }
    }

    return items;
  } catch (err) {
    console.error(err);
    return [];
  }
}

export async function getPermissionlessResidentSlot(poolIndex, slotId) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const slot = await contract.getSlot(poolIndex, slotId);
  let burn;
  let deposit;
  let rewards;

  /*
    0 - lastUpdatedBlock
    1 - depositWei
    2 - burnRateWei
    3 - rewardsWeiForSession
    4 - owner
    */
  if (slot[5] === ZERO_ADDRESS) {
    burn = deposit = "0";
    rewards = { rewards1: "0", rewards2: "0" };
  } else {
    try {
      burn = await contract.getBurn(poolIndex, slotId);
    } catch (error) {
      console.error(`slot ${slotId} error: `, error);
      burn = 0;
    }

    rewards = await getPermissionlessResidentRewards(poolIndex, slotId);
    deposit = fromWei((slot[1] - burn).toString());
  }

  const burnPerPulse = fromWei(await getPermissionlessResidentBurnPerPulse(poolIndex, slot[2]));
  const rewardsForSession = fromWei(slot[3]);
  const rewardsSum = parseFloat(rewardsForSession ?? 0) + parseFloat(rewards?.rewards1 ?? 0);
  const rewardsPerBlock = await getPermissionlessResidentCurrentReward(poolIndex);
  const rewardsPerSecond = rewardsPerBlock / secondsPerBlock;
  const tokenAddress = await getRewardTokenAddressMemoized(poolIndex);
  const rewardTokenPerLPToken = fromWei(
    await getPermissionlessResidentRewardTokenPerLPTokenMemoized(poolIndex, tokenAddress)
  );

  let apy = "0";

  if (parseFloat(rewardTokenPerLPToken) === 0 || parseFloat(deposit) === 0) {
    apy = "∞";
  } else {
    apy = ((rewardsPerSecond * secondsPerYear) / (rewardTokenPerLPToken * deposit)) * 100;
  }

  return {
    index: slotId,
    owner: slot[5],
    burnRate: fromWei(slot[2]),
    burnRatePerPulse: burnPerPulse,
    rewards1: rewards?.rewards1,
    rewards2: rewards?.rewards2,
    rewardsForSession: rewardsForSession,
    rewardsSum: rewardsSum,
    apy: apy.toString(),
    apepy: await getPermissionlessResidentAPEPYFromBurnRate(poolIndex, burnPerPulse),
    deposit: deposit,
    initialDeposit: fromWei(slot[1])
  };
}

export async function getPermissionlessResidentSymbols(poolIndex) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const [pool, pulse] = await Promise.all([contract.pools(poolIndex), contract.pulses(poolIndex)]);

  const [depositTokenSymbol, rewardTokenSymbol] = await Promise.all([
    getTokenSymbolMemoized(pool.liquidityToken),
    getTokenSymbolMemoized(pulse.rewardToken)
  ]);

  return {
    depositTokenSymbol: depositTokenSymbol,
    reward1TokenSymbol: rewardTokenSymbol,
    reward2TokenSymbol: rewardTokenSymbol
  };
}

export function calcResidentRewardForBlock(pulse, blockNum) {
  const waveLength = toBN(pulse.pulseWavelengthBlocks);
  const amplitude = toBN(pulse.pulseAmplitudeWei);
  const startBlock = toBN(pulse.pulseStartBlock);

  const pulseStartBlock = waveLength.mul(blockNum.sub(startBlock).div(waveLength)).add(startBlock);
  const pulseIndex = blockNum.sub(pulseStartBlock);
  const pulseConstant = amplitude.div(waveLength.mul(waveLength));
  const currentReward = pulseConstant.mul(
    waveLength.sub(pulseIndex).mul(waveLength.sub(pulseIndex))
  );

  return fromWei(currentReward.toString());
}

export async function getPermissionlessResidentRewardForBlock(poolIndex, blockNum) {
  const [contract, wavelength] = await Promise.all([
    ethInstance.getContract("read", poolInstance.contract),
    getWavelengthMemoized(poolIndex)
  ]);

  const pulse = await contract.pulses(poolIndex);
  const amplitude = toBN(pulse.pulseAmplitudeWei);
  const startBlock = toBN(pulse.pulseStartBlock);

  const pulseStartBlock = wavelength.mul(blockNum.sub(startBlock).div(wavelength)).add(startBlock);
  const pulseIndex = blockNum.sub(pulseStartBlock);
  const pulseConstant = amplitude.div(wavelength.sqr());
  const currentReward = pulseConstant.mul(wavelength.sub(pulseIndex).sqr());

  return fromWei(currentReward);
}

export async function getPermissionlessResidentDistanceToPulseEnd(poolIndex) {
  const [contract, blockNum] = await Promise.all([
    ethInstance.getContract("read", poolInstance.contract),
    getCurrentBlockNumber()
  ]);

  const pulse = await contract.pulses(poolIndex);
  const pulsesPassed = Math.floor((blockNum - pulse.pulseStartBlock) / pulse.pulseWavelengthBlocks);
  const pulseEndBlock =
    pulse.pulseWavelengthBlocks * (pulsesPassed + 1) + parseInt(pulse.pulseStartBlock);

  return pulseEndBlock - blockNum;
}

export async function getPermissionlessResidentCurrentReward(poolIndex) {
  const currentBlock = toBN(await getCurrentBlockNumber());
  return await getPermissionlessResidentRewardForBlock(poolIndex, currentBlock);
}

export async function getPermissionlessResidentTotalBurned(poolIndex) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const poolStats = await contract.poolStats(poolIndex);

  return poolStats.totalBurnedWei.toString();
}

export async function getPermissionlessResidentLPBalance(tokenAddress, target) {
  const lpContract = await ethInstance.getReadContractByAddress(contracts.LPToken, tokenAddress);

  if (!target) {
    target = await ethInstance.getEthAccount(false);
  }

  const balance = fromWei(await lpContract.balanceOf(target));
  return balance;
}

export async function calcResidentTotalLiqudity(pool) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const lpBalance = await getPermissionlessResidentLPBalance(pool.liquidityToken, contract.address);

  const totalLiquidity = Math.floor(parseInt(lpBalance));
  return totalLiquidity;
}

export async function getPermissionlessResidentTotalLiquidity(poolIndex) {
  const yieldContract = await ethInstance.getContract("read", poolInstance.contract);

  const [pool, poolStats] = await Promise.all([
    yieldContract.pools(poolIndex),
    yieldContract.poolStats(poolIndex)
  ]);

  const lpBalance = await getPermissionlessResidentLPBalance(
    pool.liquidityToken,
    yieldContract.address
  );
  const burnAmount = fromWei(poolStats.totalBurnedWei);
  const totalLiquidity = Math.floor(parseInt(lpBalance) + parseInt(burnAmount));

  return totalLiquidity;
}

export async function getPermissionlessResidentRewardGraph(datas, samplingWavelength = 1) {
  const { waveLength, pulseAmplitudeWei, pulseStartBlock } = datas;
  const currentBlock = await ethInstance.getBlock("latest");
  const waveLengthBN = toBN(waveLength);
  const amplitude = toBN(pulseAmplitudeWei);
  const currentBlockNumber = toBN(currentBlock.number);
  const startBlock = toBN(pulseStartBlock);
  const avgBlockTimeSeconds = toBN("13");
  const calcPulseStartBlock = waveLengthBN
    .mul(currentBlockNumber.sub(startBlock).div(waveLengthBN))
    .add(startBlock);

  let pulseStartBlockTime = toBN(0);

  if (calcPulseStartBlock.gt(currentBlockNumber)) {
    pulseStartBlockTime = toBN(currentBlock.timestamp).add(
      calcPulseStartBlock.sub(currentBlockNumber).mul(avgBlockTimeSeconds)
    );
  } else {
    const block = await ethInstance.getBlock(parseInt(calcPulseStartBlock.toString()));
    pulseStartBlockTime = toBN(block.timestamp);
  }

  const pulseConstant = amplitude.div(waveLengthBN.mul(waveLengthBN));
  const timeSeries = [];

  for (let j = 0; j < waveLengthBN.toNumber(); j += samplingWavelength) {
    const i = toBN(j.toString());

    timeSeries.push({
      x: new Date(pulseStartBlockTime.add(i.mul(avgBlockTimeSeconds)).toString() * 1000),
      y: parseFloat(fromWei(pulseConstant.mul(waveLengthBN.sub(i).mul(waveLengthBN.sub(i)))))
    });
  }

  return timeSeries;
}

export async function getPermissionlessResidentCurrentBlockXY(datas) {
  const { waveLength, pulseAmplitudeWei, pulseStartBlock } = datas;

  const currentBlock = await ethInstance.getBlock("latest");
  const currentBlockNumber = currentBlock.number;
  const currentTimestamp = currentBlock.timestamp;
  const startBlock = toBN(pulseStartBlock);
  const pulsesPassed = Math.floor((currentBlockNumber - startBlock) / waveLength);
  const pulseEndBlock = waveLength * (pulsesPassed + 1) + parseInt(startBlock);
  const amplitude = toBN(pulseAmplitudeWei);
  const waveLengthBN = toBN(waveLength);
  const pulseConstant = amplitude.div(waveLengthBN.mul(waveLengthBN));
  const i = toBN((pulseEndBlock - currentBlockNumber).toString());

  return {
    x: new Date(currentTimestamp.toString() * 1000),
    y: parseFloat(fromWei(pulseConstant.mul(i * i)))
  };
}

export async function getPermissionlessResidentAmplitude(poolIndex) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const pulse = await contract.pulses(poolIndex);
  const amplitude = pulse.pulseAmplitudeWei;

  return fromWei(amplitude);
}

export function calcResidentRewardPerPulse(pulse) {
  const waveLength = toBN(pulse.pulseWavelengthBlocks);
  const amplitude = toBN(pulse.pulseAmplitudeWei);
  const pulseConstant = amplitude.div(waveLength.mul(waveLength));
  const pulseRewards = pulseConstant
    .mul(waveLength.mul(waveLength.add(1)))
    .mul(waveLength.mul(2).add(1))
    .div(6);

  return pulseRewards;
}

export async function getPermissionlessResidentRewardsPerPulse(poolIndex) {
  const [contract, wavelength] = await Promise.all([
    ethInstance.getContract("read", poolInstance.contract),
    getWavelengthMemoized(poolIndex)
  ]);

  const pulse = await contract.pulses(poolIndex);
  const amplitude = toBN(pulse.pulseAmplitudeWei);
  const pulseConstant = amplitude.div(wavelength.mul(wavelength));
  const pulseRewards = pulseConstant
    .mul(wavelength.mul(wavelength.add(1)))
    .mul(wavelength.mul(2).add(1))
    .div(6);

  return pulseRewards;
}

const getRewardsPerPulseMemoized = memoize(getPermissionlessResidentRewardsPerPulse);

export async function getPermissionlessResidentRewardTokenAddress(poolIndex) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const pulse = await contract.pulses(poolIndex);
  const rewardTokenAddress = pulse.rewardToken;

  return rewardTokenAddress;
}

const getRewardTokenAddressMemoized = memoize(getPermissionlessResidentRewardTokenAddress);

export function calcResidentBurnPerPulse(waveLength, burnPerBlock) {
  const waveLengthBN = toBN(waveLength);
  const burnBN = toBN(burnPerBlock);
  return waveLengthBN.mul(burnBN);
}

export async function getPermissionlessResidentBurnPerPulse(poolIndex, burnPerBlock) {
  const wavelength = await getWavelengthMemoized(poolIndex);
  const burnBN = toBN(burnPerBlock);

  return wavelength.mul(burnBN);
}

export async function getPermissionlessResidentBurnPerBlock(poolIndex, burnPerPulse) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const pulses = await contract.pulses(poolIndex);
  const burnBN = toBN(burnPerPulse);

  return burnBN.div(pulses.pulseWavelengthBlocks);
}

export async function getPermissionlessResidentSlots(poolIndex) {
  const maxStakers = parseInt(await getMaxPermissionlessResidentStakersMemoized(poolIndex));
  let slots = [];
  for (let index = 1; index <= maxStakers; index++) {
    slots.push([poolIndex, index]);
  }

  const [
    slotsResult,
    rewardsResult,
    burnsResult,
    pulse
  ] = await getPermissionlessResidentSlotsMulti(poolIndex, slots);

  // calc rewards per block
  const blockNum = toBN(await getCurrentBlockNumber());
  const waveLength = toBN(pulse[0].pulseWavelengthBlocks);
  const tokenAddress = pulse[0].rewardToken;
  const rewardsPerBlock = calcResidentRewardForBlock(pulse[0], blockNum);

  const rewardTokenPerLPToken = fromWei(
    await getPermissionlessResidentRewardTokenPerLPTokenMemoized(poolIndex, tokenAddress)
  );

  const results = slotsResult.map((slot, index) => {
    let burn;
    let deposit;
    let rewards;

    if (slot[5] === ZERO_ADDRESS) {
      burn = deposit = "0";
      rewards = ["0", "0"];
    } else {
      try {
        burn = burnsResult[index][0];
      } catch (error) {
        console.error(`slot ${index + 1} error: `, error);
        burn = 0;
      }

      rewards = rewardsResult[index];
      deposit = fromWei((slot[1] - burn).toString());
    }

    const burnPerPulse = fromWei(waveLength.mul(toBN(slot[2])));
    const rewardsForSession = fromWei(slot[3]);
    const rewardsSum = parseFloat(rewardsForSession ?? 0) + parseFloat(fromWei(rewards?.[0] ?? 0));
    const rewardsPerSecond = rewardsPerBlock / secondsPerBlock;

    //calc apy
    let apy = "0";
    if (parseFloat(rewardTokenPerLPToken) === 0 || parseFloat(deposit) === 0) {
      apy = "∞";
    } else {
      apy = ((rewardsPerSecond * secondsPerYear) / (rewardTokenPerLPToken * deposit)) * 100;
    }

    //calc apepy
    const fvtPerLP = toWei(rewardTokenPerLPToken);
    const pulseRewards = calcResidentRewardPerPulse(pulse[0]);
    const apepy = calcResidentAPEPYFromBurnRate(fvtPerLP, burnPerPulse, pulseRewards);

    const rs = {
      index: index + 1,
      owner: slot[5],
      burnRate: fromWei(slot[2]),
      burnRatePerPulse: burnPerPulse,
      rewards1: fromWei(rewards?.[0]),
      rewards2: fromWei(rewards?.[0]),
      rewardsForSession: rewardsForSession,
      rewardsSum: rewardsSum,
      apy: apy.toString(),
      apepy: apepy,
      deposit: deposit,
      initialDeposit: fromWei(slot[1]),
      lastUpdatedBlock: slot[0]
    };

    return rs;
  });

  const list = { all: [], occupied: [] };

  results.forEach((x) => {
    if (x.owner !== ZERO_ADDRESS) {
      x.vacant = false;
      list.occupied.push(x);
    } else {
      x.vacant = true;
    }

    list.all.push(x);
  });
  return list;
}

export async function getPermissionlessResidentAverageAPEPY(slotIds) {
  if (slotIds && slotIds.occupied) {
    const apepys = slotIds.occupied.map((x) => x.apepy);

    if (apepys.some((x) => x === "∞")) {
      return "∞";
    }

    return apepys.length === 0
      ? "∞"
      : apepys.reduce((a, b) => parseFloat(a) + parseFloat(b), 0) / apepys.length;
  } else {
    return "∞";
  }
}

export async function getPermissionlessResidentAverageAPY(slotIds) {
  if (slotIds && slotIds.occupied) {
    const apys = slotIds.occupied.map((x) => x.apy);

    if (apys.some((x) => x === "∞")) {
      return "∞";
    }

    return apys.length === 0
      ? "∞"
      : apys.reduce((a, b) => parseFloat(a) + parseFloat(b), 0) / apys.length;
  } else {
    return "∞";
  }
}

export async function getPermissionlessResidentRewardTokenPerLPToken(
  poolIndex,
  rewardTokenAddress
) {
  const resContract = await ethInstance.getContract("read", poolInstance.contract);
  const pool = await resContract.pools(poolIndex);

  const contract = await ethInstance.getReadContractByAddress(
    contracts.LPToken,
    pool.liquidityToken
  );

  const [token0, token1] = await Promise.all([contract.token0(), contract.token1()]);
  let reserveIndex = 0;

  if (token1.toLowerCase() === rewardTokenAddress.toLowerCase()) {
    reserveIndex = 1;
  } else if (token0.toLowerCase() !== rewardTokenAddress.toLowerCase()) {
    return toBN(0);
  }

  const [reserves, totalSupply] = await Promise.all([
    contract.getReserves(),
    contract.totalSupply()
  ]);

  return toBN(reserves[reserveIndex])
    .mul(toBN(toWei("2")))
    .div(toBN(totalSupply));
}

const getPermissionlessResidentRewardTokenPerLPTokenMemoized = memoize(
  getPermissionlessResidentRewardTokenPerLPToken
);

export function calcResidentAPEPYFromBurnRate(fvtPerLP, lpBurnPerPulse, rewardsPerPulse) {
  let fvtBurnPerPulse = toBN(fvtPerLP.toString())
    .mul(toBN(toWei(lpBurnPerPulse.toString())))
    .div(toBN((1e18).toString())); // this division is only to remove the additional multiplication done by parsing lpBurnPerPulse to WEI
  fvtBurnPerPulse = fromWei(fvtBurnPerPulse.toString()); //revert WEI from base fvtPerLP
  fvtBurnPerPulse = toBN(fvtBurnPerPulse.split(".")[0]); // similar to math.floor needed to be able to convert the number to BN
  if (fvtBurnPerPulse.eq(0)) {
    if (rewardsPerPulse.eq(0)) {
      return "0";
    } else {
      return "∞";
    }
  } else {
    const apepy = fromWei(rewardsPerPulse.mul(toBN("100")).div(fvtBurnPerPulse).toString());
    return apepy;
  }
}

export async function getPermissionlessResidentAPEPYFromBurnRate(poolIndex, lpBurnPerPulse) {
  const [rewardTokenAddress, rewardsPerPulse] = await Promise.all([
    getRewardTokenAddressMemoized(poolIndex),
    getRewardsPerPulseMemoized(poolIndex)
  ]);

  const fvtPerLP = await getPermissionlessResidentRewardTokenPerLPTokenMemoized(
    poolIndex,
    rewardTokenAddress
  );
  const result = calcResidentAPEPYFromBurnRate(fvtPerLP, lpBurnPerPulse, rewardsPerPulse);
  return result;
}

export async function getPermissionlessResidentClaimMinimums(poolIndex, slotIndex) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const result = await contract.getClaimMinimums(poolIndex, slotIndex);

  return { minDeposit: result[0], minBurnRate: result[1] };
}

export async function getPermissionlessResidentAPEPYFromSlot(poolIndex, slotIndex) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const [wavelength, slot] = await Promise.all([
    getWavelengthMemoized(poolIndex),
    contract.contract.getSlot(poolIndex, slotIndex) //0 - lastUpdatedBlock, 1 - depositWei, 2 - burnRateWei, 3 - rewardsForSession, 4 - owner
  ]);

  return getPermissionlessResidentAPEPYFromBurnRate(
    poolIndex,
    fromWei(wavelength.mul(toBN(slot[2])).toString())
  );
}

export async function getPermissionlessResidentRewardsForSession(poolIndex, slotId) {
  const contract = await ethInstance.getContract("read", poolInstance.contract);
  const [rewards, notYetClaimed] = await Promise.all([
    contract.rewardsForSession(poolIndex, slotId),
    getPermissionlessResidentRewards(poolIndex, slotId)
  ]);

  const rewardForSession = fromWei(rewards);

  return rewardForSession + notYetClaimed.rewards1;
}

export async function getPermissionlessResident24hrsVolume(poolIndex, callback) {
  const blocksPerDay = secondsPerDay / secondsPerBlock;
  const currentBlock = (await ethInstance.getBlock("latest")).number;
  const fromBlock = Math.floor(currentBlock - blocksPerDay);

  const filters = { filter: [+poolIndex], fromBlock: fromBlock };
  try {
    const filteredEvents = await ethInstance.getAllEvents(
      async (event) => {
        const data = { ...event.args, transactionHash: event.transactionHash };
        if (data.depositWei) {
          const pardesData = parseBNData(data, RESULT_TYPES.OBJECT, true);
          const deposit = fromWei(pardesData.depositWei);
          callback(deposit);
        }
      },
      poolInstance.contract,
      "SlotChangedHands",
      filters
    );

    const parsedData = filteredEvents.map((x) => {
      return parseBNData(x.args, RESULT_TYPES.OBJECT, true);
    });

    const calcDeposit = parsedData
      .map((x) => parseFloat(fromWei(x.depositWei)))
      .reduce((a, b) => a + b, 0);

    callback(calcDeposit);
  } catch (error) {
    console.error(error);
  }
}

export async function getPermissionlessResidentAccountAllowance(tokenAddress) {
  let currentApproval = "0";
  if (tokenAddress) {
    const { account, isConnected } = await accountConnected();
    if (isConnected) {
      const [liquidity, token] = await Promise.all([
        getPermissionlessResidentWriteContract(),
        ethInstance.getWriteContractByAddress(contracts.LPToken, tokenAddress)
      ]);

      try {
        currentApproval = await token.allowance(account, liquidity.address);
      } catch (error) {
        //changing chain error
        console.log("getPermissionlessResidentAllowace fail: ", error);
      }
    }
  }

  const result = parseFloat(fromWei(currentApproval));
  return result;
}

export async function getUserStats(poolIndex) {
  let userStats = {
    0: "0",
    1: "0",
    2: "0"
  };

  const { account, isConnected } = await accountConnected();

  if (isConnected) {
    const contract = await ethInstance.getContract("read", poolInstance.contract);
    userStats = await contract.getUserStats(poolIndex, account);
  }

  return {
    totalStaked: fromWei(userStats[0]),
    totalRewards: fromWei(userStats[1]),
    totalBurned: fromWei(userStats[2])
  };
}

export async function addPermissionlessResidentPool(pool) {
  const liquidity = await getPermissionlessResidentWriteContract();
  let newPoolTransaction = null;
  let configTransaction = null;

  try {
    const newPool = await liquidity.addPool(
      pool.rewardToken1Addr,
      pool.rewardToken2Addr,
      pool.liquidityTokenAddr,
      pool.taxAddr,
      pool.poolOwner,
      toWei(pool.rewardPerBlock2Wei.toString()),
      pool.pulseStartDelayBlocks,
      getBytes(pool.ipfsHash),
      stringToBytes(pool.name)
    );

    newPoolTransaction = await newPool.wait();

    if (newPoolTransaction.status === TRANSACTION_STATUS.CONFIRMED) {
      const poolIndex = newPoolTransaction.events
        ?.find((x) => x.event === "PoolAdded")
        .args?.poolId.toString();

      const config = await liquidity.setConfig(
        poolIndex,
        pool.mxStkrs,
        toWei(pool.minDepositWei.toString()),
        toWei(pool.maxDepositWei.toString()),
        toWei(pool.minBurnRateWei.toString()),
        toWei(pool.maxBurnRateWei.toString()),
        pool.pulseLengthBlocks,
        toWei(pool.pulseAmplitudeWei.toString())
      );

      configTransaction = await config.wait();
    }
  } catch (error) {
    console.error("addPermissionlessResidentPool: ", error);
  }

  return (
    newPoolTransaction.status === TRANSACTION_STATUS.CONFIRMED &&
    configTransaction.status === TRANSACTION_STATUS.CONFIRMED
  );
}

export async function unpausePermissionlessResidentPool(poolIndex) {
  const liquidity = await getPermissionlessResidentWriteContract();
  const res = await liquidity.unpausePool(poolIndex);
  return await res.wait();
}

export async function getPermissionlessResidentSlotsMulti(poolIndex, slots) {
  try {
    const jsonResident = poolInstance.contract;
    const chainId = ethInstance.getChainId();
    const address = jsonResident.networks[chainId].address;

    const methods = ["getSlot", "getRewards", "getBurn", "pulses"];
    const slotMethods = ["getSlot", "getRewards", "getBurn"];

    const calls = slotMethods.map((name) => {
      return { name: name, params: slots, resultType: RESULT_TYPES.ARRAY };
    });

    calls.push({
      name: "pulses",
      params: [[poolIndex]],
      resultType: RESULT_TYPES.OBJECT
    });

    if (calls.length) {
      const contract = await ethInstance.getContract("read", contracts.Multicall);
      const interf = getInterface(poolInstance.contract.abi);

      const groups = calls.reduce((acc, value) => {
        if (!acc[value.name]) acc[value.name] = { name: "", calls: [] };

        acc[value.name].name = value.name;
        acc[value.name].calls = value.params.map((params) => [
          address,
          interf.encodeFunctionData(value.name, params)
        ]);

        return acc;
      }, {});

      const response = await Promise.all(
        Object.values(groups).map((x) => contract.aggregate(x.calls))
      );

      const decodedResponse = response.map((group, index) => {
        return group.returnData.map((x) => {
          return interf.decodeFunctionResult(methods[index], x);
        });
      });

      return decodedResponse.map((x, index) => {
        return parseBNData(x, calls[index].resultType);
      });
    }
    return [];
  } catch (e) {
    console.log(e);
    return Promise.reject(e);
  }
}

export async function getPermissionlessResidentGlobalsMulti(poolIndex) {
  try {
    const jsonResident = poolInstance.contract;
    const chainId = ethInstance.getChainId();

    const methods = ["pulses", "pools", "poolStats", "metadatas"];
    const address = jsonResident.networks[chainId].address;
    const requests = methods.map((name) => {
      return {
        name: name,
        params: [poolIndex]
      };
    });

    if (requests.length) {
      const contract = await ethInstance.getReadContractByAddress(
        contracts.Multicall,
        contracts.Multicall.networks[chainId].address
      );

      const interf = getInterface(poolInstance.contract.abi);

      const calls = requests.map((call) => {
        return [address, interf.encodeFunctionData(call.name, call.params)];
      });

      const response = await contract.aggregate(calls, {});

      const decodedResponse = response.returnData.map((resp, index) => {
        return interf.decodeFunctionResult(methods[index], resp);
      });

      const resp = decodedResponse.map((x) => {
        return parseBNData(x, RESULT_TYPES.OBJECT, true);
      });

      return resp;
    }
  } catch (e) {
    console.log(e);
    return Promise.reject(e);
  }
}
