import { bytesToString, ethInstance, fromWei, stringToBytes } from "evm-chain-scripts";
import moment from "moment";
import specialPools from "specialPools.json";
import {
  getDepositToken,
  getPoolInstance,
  getReceiptByType,
  getWriteContractByType,
  inputToArray
} from "utils/helper";
import {
  BASIC_CONTRACT_TYPE,
  calcBasicFromTime,
  getBytes,
  getRewardTokenPerLPToken,
  getTokenSymbol,
  secondsPerYear
} from "./poolHelper";

export async function getPermissionlessBasicMaturityRewards(receiptId, poolIndex) {
  const { contract } = getPoolInstance(BASIC_CONTRACT_TYPE.PERMISSIONLESS);
  const yieldContract = await ethInstance.getContract("read", contract);
  const [pool, rewardData, receipt] = await Promise.all([
    yieldContract.pools(poolIndex),
    yieldContract.getRewardData(poolIndex), // 0-rewardsWeiPerSecondPerToken[], 1-rewardsWeiClaimed[], 2-rewardTokens[]
    getReceiptByType(BASIC_CONTRACT_TYPE.PERMISSIONLESS, receiptId, poolIndex)
  ]);

  const timeSecDiff = pool.endTime - receipt[1];
  const rewardsWeiPerSecondPerToken = fromWei(rewardData[0][0]);
  const depositAmount = fromWei(receipt[0]);
  const rewardAtMaturity = timeSecDiff * rewardsWeiPerSecondPerToken * depositAmount;
  return rewardAtMaturity;
}

export async function calcPermissionlessBasicFromTime(
  pool,
  startTime,
  rewardsData,
  end = null,
  lpToken = false
) {
  const endTime = end || pool.endTime;
  const seconds = endTime - startTime;
  const tokensPerSecondPerToken = fromWei(rewardsData && rewardsData[0] ? rewardsData[0][0] : "0");

  let tokensPerToken = tokensPerSecondPerToken * seconds * 100;
  if (lpToken) {
    const rewardsPerLP = await getRewardTokenPerLPToken(pool.depositToken, rewardsData[2][0]);
    tokensPerToken /= fromWei(rewardsPerLP.toString());
  }

  return tokensPerToken.toString();
}

export async function getPermissionlessBasicFromTime(
  startTime,
  poolIndex,
  end = null,
  lpToken = false
) {
  const { contract } = getPoolInstance(BASIC_CONTRACT_TYPE.PERMISSIONLESS);
  const yieldContract = await ethInstance.getContract("read", contract);

  const [pool, rewardsData] = await Promise.all([
    yieldContract.pools(poolIndex),
    yieldContract.getRewardData(poolIndex) // 0-rewardsWeiPerSecondPerToken[], 1-rewardsWeiClaimed[], 2-rewardTokens[]
  ]);

  const tokensPerToken = await calcBasicFromTime(pool, startTime, rewardsData, end, lpToken);

  return tokensPerToken.toString();
}

export async function getBasicPoolMaxNumber() {
  const yieldContract = await getWriteContractByType(BASIC_CONTRACT_TYPE.PERMISSIONLESS);
  return await yieldContract.numPools();
}

export async function getPermissionlessBasicAccountBalance(poolIndex) {
  if (!poolIndex || poolIndex.startsWith("0x")) return;
  let balance = 0;
  try {
    const [tokenContract, account] = await Promise.all([
      getDepositToken(BASIC_CONTRACT_TYPE.PERMISSIONLESS, poolIndex),
      ethInstance.getEthAccount(false)
    ]);
    balance = fromWei(await tokenContract.balanceOf(account));
  } catch (error) {
    console.log("Error while getting account balance:", error);
  }

  return balance;
}

export async function getAllPermissionlessBasicTokensSymbols(poolIndex) {
  const { contract } = getPoolInstance(BASIC_CONTRACT_TYPE.PERMISSIONLESS);

  const yieldContract = await ethInstance.getContract("read", contract);
  const [pool, rewardData] = await Promise.all([
    yieldContract.pools(poolIndex),
    yieldContract.getRewardData(poolIndex)
  ]);

  const reward1Token = rewardData[2][0];
  const reward2Token = rewardData[2].length > 1 ? rewardData[2][1] : null;

  const [depositTokenSymbol, reward1TokenSymbol, reward2TokenSymbol] = await Promise.all([
    getTokenSymbol(pool.depositToken),
    reward1Token ? getTokenSymbol(reward1Token) : "",
    reward2Token ? getTokenSymbol(reward2Token) : ""
  ]);

  return {
    depositTokenSymbol: depositTokenSymbol,
    reward1TokenSymbol: reward1TokenSymbol,
    reward2TokenSymbol: reward2TokenSymbol
  };
}

export async function getPermissionlessBasicGlobals(poolIndex, lpToken = false) {
  if (!poolIndex) return;
  const { chainId, contract } = getPoolInstance(BASIC_CONTRACT_TYPE.PERMISSIONLESS);
  try {
    const yieldContract = await ethInstance.getContract("read", contract);

    const [pool, rewardData, poolMetadata] = await Promise.all([
      yieldContract.pools(poolIndex),
      yieldContract.getRewardData(poolIndex), // 0-rewardsWeiPerSecondPerToken[], 1-rewardsWeiClaimed[], 2-rewardTokens[]
      yieldContract.metadatas(poolIndex)
    ]);

    // pool not exist
    if (parseInt(pool.id) === 0) return;

    if (specialPools.lpBasic[chainId]?.includes(parseInt(poolIndex))) lpToken = true;

    const tokensPerSecondPerToken = fromWei(rewardData[0][0] ?? "0");
    // TODO: don't assume first reward token is same as deposit token
    const maxRewards = tokensPerSecondPerToken * secondsPerYear;

    let now = Math.floor(moment.now() / 1000);

    let yldCurrent = 0;

    if (now <= pool.endTime) {
      if (now < pool.startTime) {
        now = pool.startTime;
      }
      const yld = await calcBasicFromTime(pool, now, rewardData, null, lpToken);

      yldCurrent = parseFloat(yld);
    }

    let apy = maxRewards * 100;

    if (lpToken) {
      const rewardsPerLP = await getRewardTokenPerLPToken(pool.depositToken, rewardData[2][0]);
      apy /= fromWei(rewardsPerLP.toString());
    }
    const isFinished = pool.endTime <= Math.floor(Date.now() / 1e3);

    return {
      maxPoolSize: fromWei(pool.maximumDepositWei),
      literalAPY: apy,
      basicTVL: parseFloat(fromWei(pool.totalDepositsWei)),
      dates: { startTime: pool.startTime, endTime: pool.endTime },
      currentYield: yldCurrent,
      name: `${bytesToString(poolMetadata.name)} Pool${isFinished ? " (Finished)" : ""}`,
      depositToken: pool.depositToken
    };
  } catch (error) {
    console.log("Error while getting permissionelss basic pool");
  }
}

export async function getPermissionlessBasicGlobalsAll(light = false) {
  try {
    const poolInstance = getPoolInstance(BASIC_CONTRACT_TYPE.PERMISSIONLESS);
    const { contract, chainId } = poolInstance;

    if (!contract.networks[chainId] || !contract.networks[chainId].address) return [];

    const yieldContract = await ethInstance.getContract("read", contract);
    const poolIndexMax = Number(await yieldContract.numPools());
    const items = [];

    for (let index = 1; index <= poolIndexMax; index++) {
      if (specialPools.ignoredPermissionlessBasic[chainId]?.includes(index)) continue;

      let lpToken = false;

      if (specialPools.lpBasic[chainId]?.includes(index)) lpToken = true;

      const [pool, poolMetadata, rewardData] = await Promise.all([
        yieldContract.pools(index),
        yieldContract.metadatas(index),
        yieldContract.getRewardData(index)
      ]);

      const isFinished = pool.endTime <= Math.floor(Date.now() / 1e3);

      if (light) {
        items.push({
          index: index,
          maxPoolSize: fromWei(pool.maximumDepositWei),
          basicTVL: parseFloat(fromWei(pool.totalDepositsWei)),
          dates: { startTime: pool.startTime, endTime: pool.endTime },
          depositToken: pool.depositToken,
          name: `${bytesToString(poolMetadata.name)}${isFinished ? " (Finished)" : ""}`
        });
      } else {
        const tokensPerSecondPerToken = fromWei(rewardData[0][0]);
        const maxRewards = tokensPerSecondPerToken * secondsPerYear;

        let now = Math.floor(moment.now() / 1000);
        let yldCurrent = 0;

        if (now <= pool.endTime) {
          if (now < pool.startTime) {
            now = pool.startTime;
          }
          const yld = await calcBasicFromTime(pool, now, rewardData, null, lpToken);

          yldCurrent = parseFloat(yld);
        }

        let apy = maxRewards * 100;

        if (lpToken) {
          const rewardsPerLP = await getRewardTokenPerLPToken(pool.depositToken, rewardData[2][0]);
          apy /= fromWei(rewardsPerLP.toString());
        }

        items.push({
          index: index,
          maxPoolSize: fromWei(pool.maximumDepositWei),
          literalAPY: apy,
          basicTVL: parseFloat(fromWei(pool.totalDepositsWei)),
          dates: { startTime: pool.startTime, endTime: pool.endTime },
          currentYield: yldCurrent,
          depositToken: pool.depositToken,
          name: `${bytesToString(poolMetadata.name)}${isFinished ? " (Finished)" : ""}`
        });
      }
    }

    return items;
  } catch (err) {
    console.error(err);
    return [];
  }
}

export async function addPermissionlessBasicPool(pool) {
  const basicPool = await getWriteContractByType(BASIC_CONTRACT_TYPE.PERMISSIONLESS);

  const rewardsWeiPerSecondPerToken = Array.isArray(pool.rewardsWeiPerSecondPerToken)
    ? pool.rewardsWeiPerSecondPerToken
    : inputToArray(pool.rewardsWeiPerSecondPerToken);

  const rewardTokenAddresses = Array.isArray(pool.rewardTokenAddresses)
    ? pool.rewardTokenAddresses
    : inputToArray(pool.rewardTokenAddresses);

  return await basicPool.addPool(
    pool.startTime,
    pool.maxDeposit,
    rewardsWeiPerSecondPerToken,
    pool.programLengthDays,
    pool.depositTokenAddress,
    pool.excessBeneficiary,
    rewardTokenAddresses,
    getBytes(pool.ipfsHash),
    stringToBytes(pool.name)
  );
}
