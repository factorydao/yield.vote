import Pool from "components/pool";
import { MainContext } from "context/context";
import Page from "pages/Page";
import partnersJSON from "partners.json";
import PropTypes from "prop-types";
import { memo, useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { getAllPartnersPools } from "utils/helper";
import { getResidentGlobals } from "utils/residentPool";

function PoolPage() {
  const { poolIndex } = useParams();
  const { currentNetwork, isMobile } = useContext(MainContext);
  const [partnerStyles, setPartnerStyles] = useState();
  let history = useNavigate();

  useEffect(() => {
    (async () => {
      const pool = await getResidentGlobals(poolIndex, currentNetwork);

      if (pool) {
        for (let index = 0; index < partnersJSON.length; index++) {
          const partner = partnersJSON[index];
          const exists = partner.depositTokens.find(
            (x) => x.toLowerCase() === pool.liquidityToken.toLowerCase()
          );

          if (exists) {
            setPartnerStyles(partner.theme);
            break;
          }
        }
      } else {
        const partnersPools = await getAllPartnersPools(true, currentNetwork);

        for (const partner of partnersJSON) {
          const firstPool = partnersPools.find((partnerPool) => partnerPool.key === partner.key)
            .residentPools[0];

          // redirect for existing pool in specific chain or on the home page when pool not exist
          if (firstPool) history.replace(`/pool/resident/${firstPool.index}`);
          else history.replace("/");

          break;
        }
      }
    })();
  }, [poolIndex, isMobile, currentNetwork]);

  return (
    <Page
      poolMenu={true}
      partnerStyles={partnerStyles}
      fluid={true}
      rightContent={<Pool partnerStyles={partnerStyles} poolIndex={poolIndex} />}
    />
  );
}

PoolPage.propTypes = {
  className: PropTypes.string
};

export default memo(PoolPage);
