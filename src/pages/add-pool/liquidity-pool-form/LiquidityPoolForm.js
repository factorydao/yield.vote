import Spinner from "components/indicators/Spinner";
import { Form, Formik } from "formik";
import PropTypes from "prop-types";
import { memo, useState } from "react";
import Button from "react-bootstrap/Button";
import { toast } from "react-toastify";
import { addResidentPool } from "utils/residentPool";
import * as Yup from "yup";
import FormInput from "../form-input";

const requiredText = "Field is required";
const positiveText = "Value must be a positive number";
const notAddressText = "It is not a token address";
const notWalletAddress = "It is not a wallet address";

const AddLiquidityPoolForm = ({ checkTokenAddress }) => {
  const [isLoading, setIsLoading] = useState(false);

  const inputsValidation = Yup.object({
    rewardToken1Addr: Yup.string()
      .required(requiredText)
      .test(
        "isAddress",
        notAddressText,
        async (address) => (await checkTokenAddress(address)) !== "0x"
      ),
    rewardToken2Addr: Yup.string()
      .required(requiredText)
      .test(
        "isAddress",
        notAddressText,
        async (address) => (await checkTokenAddress(address)) !== "0x"
      ),
    liquidityTokenAddr: Yup.string()
      .required(requiredText)
      .test(
        "isAddress",
        notAddressText,
        async (address) => (await checkTokenAddress(address)) !== "0x"
      ),
    taxAddr: Yup.string().required(requiredText),
    poolOwner: Yup.string()
      .required(requiredText)
      .test(
        "isWalletAddress",
        notWalletAddress,
        async (address) => (await checkTokenAddress(address)) === "0x"
      ),
    rewardPerBlock2Wei: Yup.number().positive(positiveText).required(requiredText),
    pulseStartDelayBlocks: Yup.number().min(0).required(requiredText),
    pulseLengthBlocks: Yup.number().positive(positiveText).required(requiredText),
    pulseAmplitudeWei: Yup.number().positive(positiveText).required(requiredText),
    mxStkrs: Yup.number().positive(positiveText).max(1000).required(requiredText),
    minBurnRateWei: Yup.number().positive(positiveText).required(requiredText),
    maxBurnRateWei: Yup.number().positive(positiveText).required(requiredText),
    minDepositWei: Yup.number().min(0).required(requiredText),
    maxDepositWei: Yup.number().positive(positiveText).required(requiredText),
    ipfsHash: Yup.string().required(requiredText),
    name: Yup.string().required(requiredText)
  });

  const handleSubmit = async (values) => {
    try {
      setIsLoading(true);
      const isAdded = await addResidentPool(values);
      if (isAdded) {
        toast.success(`The new pool has been added!`, {
          position: "top-center"
        });
      } else {
        toast.error("The new pool not added ", { position: "top-center" });
      }
    } catch (e) {
      toast.error(e, { position: "top-center" });
      console.error(e);
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <Formik
      initialValues={{
        rewardToken1Addr: "",
        rewardToken2Addr: "",
        liquidityTokenAddr: "",
        taxAddr: "0xa874Fa6ccDcCB57d9397247e088575C4EF34EC66",
        poolOwner: "",
        rewardPerBlock2Wei: "",
        pulseStartDelayBlocks: "",
        pulseLengthBlocks: "",
        pulseAmplitudeWei: "",
        mxStkrs: "",
        minBurnRateWei: "",
        maxBurnRateWei: "",
        minDepositWei: "",
        maxDepositWei: "",
        ipfsHash: "",
        name: ""
      }}
      validationSchema={inputsValidation}
      onSubmit={handleSubmit}>
      {({ errors }) => {
        return !isLoading ? (
          <Form>
            <FormInput
              label="Reward token 1 address"
              name="rewardToken1Addr"
              type="text"
              placeholder="Please enter reward token 1 address"
            />
            <FormInput
              label="Reward token 2 address"
              name="rewardToken2Addr"
              type="text"
              placeholder="Please enter reward token 2 address"
            />
            <FormInput
              label="Liquidity token address"
              name="liquidityTokenAddr"
              type="text"
              placeholder="Please enter liquidity token address"
            />
            <FormInput
              label="Tax address"
              name="taxAddr"
              type="text"
              placeholder="Please enter tax address"
            />
            <FormInput
              label="Pool owner"
              name="poolOwner"
              type="text"
              placeholder="Please enter pool owner"
            />
            <FormInput
              label="Reward per block 2 (in Wei)"
              name="rewardPerBlock2Wei"
              type="number"
              placeholder="Please enter reward per block 2"
            />
            <FormInput
              label="Pulse start delay"
              name="pulseStartDelayBlocks"
              type="number"
              placeholder="Please enter pulse start delay"
            />
            <FormInput
              label="Pulse length blocks"
              name="pulseLengthBlocks"
              type="number"
              placeholder="Please enter pulse length blocks"
            />
            <FormInput
              label="Pulse amplitude (in Wei)"
              name="pulseAmplitudeWei"
              type="number"
              placeholder="Please enter pulse amplitude"
            />
            <FormInput
              label="mxStkrs"
              name="mxStkrs"
              type="number"
              placeholder="Please enter mxStkrs"
            />
            <FormInput
              label="Minimum Burn Rate (in Wei)"
              name="minBurnRateWei"
              type="number"
              placeholder="Please enter minimum Burn Rate"
            />
            <FormInput
              label="Maximum Burn Rate (in Wei)"
              name="maxBurnRateWei"
              type="number"
              placeholder="Please enter maximum Burn Rate"
            />
            <FormInput
              label="Minimum deposit amount (in Wei)"
              name="minDepositWei"
              type="number"
              placeholder="Please enter minimum deposit amount"
            />
            <FormInput
              label="Maximum deposit amount (in Wei)"
              name="maxDepositWei"
              type="number"
              placeholder="Please enter maximum deposit amount"
            />
            <FormInput
              label="IPFS Hash"
              name="ipfsHash"
              type="text"
              placeholder="Please enter IPFS Hash"
            />
            <FormInput label="Name" name="name" type="text" placeholder="Please enter name" />
            <Button
              type="submit"
              variant="outline-secondary send-button"
              disabled={errors.length > 0 || isLoading}>
              Submit liquidity pool
            </Button>
          </Form>
        ) : (
          <Spinner overlay={true} />
        );
      }}
    </Formik>
  );
};

AddLiquidityPoolForm.propTypes = {
  checkTokenAddress: PropTypes.func
};

export default memo(AddLiquidityPoolForm);
