import PoolBasic from "components/pool-basic";
import { MainContext } from "context/context";
import Page from "pages/Page";
import partnersJSON from "partners.json";
import PropTypes from "prop-types";
import { memo, useContext, useEffect, useState } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { getAllBasicTokensSymbols, getBasicGlobals } from "utils/basicPool";
import { getAllPartnersPools } from "utils/helper";
import {
  getAllPermissionlessBasicTokensSymbols,
  getPermissionlessBasicGlobals
} from "utils/permissionlessBasicPool";
import { BASIC_CONTRACT_TYPE, contracts } from "utils/poolHelper";

function PoolBasicPage({ contractType }) {
  const { poolIndex } = useParams();
  const [pool, setPool] = useState({});
  const { isMobile, currentNetwork } = useContext(MainContext);
  const [styles, setStyles] = useState();
  const [pageLoading, setPageLoading] = useState(false);

  const location = useLocation();
  const history = useNavigate();

  useEffect(() => {
    (async () => {
      setPageLoading(true);
      let pool = {};
      let styles = {};

      switch (contractType) {
        case BASIC_CONTRACT_TYPE.BASIC:
          pool = await getBasicGlobals(poolIndex);
          break;
        case BASIC_CONTRACT_TYPE.PERMISSIONLESS:
          pool = await getPermissionlessBasicGlobals(poolIndex);
          break;
      }

      styles = partnersJSON[0].theme;
      if (pool) {
        for (let index = 0; index < partnersJSON.length; index++) {
          const partner = partnersJSON[index];
          const exist = partner.depositTokens.find((x) => x === pool.depositToken);
          if (exist) {
            styles = partner.theme;
          }
        }

        const symbols =
          contractType === BASIC_CONTRACT_TYPE.BASIC
            ? await getAllBasicTokensSymbols(poolIndex)
            : await getAllPermissionlessBasicTokensSymbols(poolIndex);

        setPool({
          ...pool,
          symbols: symbols,
          value: poolIndex ?? 1,
          contract:
            contractType === BASIC_CONTRACT_TYPE.BASIC
              ? contracts.BasicPoolFactory
              : contracts.PermissionlessBasicPoolFactory
        });
      } else {
        // pool id not exist, get all existing pools
        const partnersPools = await getAllPartnersPools(true, currentNetwork);
        const isPermissionless = location.pathname.includes("permissionless");

        for (const partner of partnersJSON) {
          const firstPool = isPermissionless
            ? partnersPools.find((partnerPool) => partnerPool.key === partner.key)
                .permissionlessBasicPools[0]
            : partnersPools.find((partnerPool) => partnerPool.key === partner.key).basicPools[0];

          // redirect for existing pool in specific chain or on the home page when pool not exist
          if (firstPool) history(`/pool/permissionless-basic/${firstPool.index}`);
          else history("/");

          break;
        }
      }

      setStyles(styles);
      setPageLoading(false);
    })();
  }, [poolIndex, isMobile, currentNetwork]);

  return (
    <Page
      poolMenu={true}
      partnerStyles={styles}
      fluid={true}
      rightContent={
        <PoolBasic pageLoading={pageLoading} poolInstance={pool} poolType={contractType} />
      }
    />
  );
}

PoolBasicPage.propTypes = {
  className: PropTypes.string
};

export default memo(PoolBasicPage);
