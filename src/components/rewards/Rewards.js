import { Filler, TimeSeriesScale, Tooltip } from "chart.js";
import Chart from "chart.js/auto";
import "chartjs-adapter-date-fns";
import Spinner from "components/indicators/Spinner";
import PropTypes from "prop-types";
import { memo, useEffect, useRef, useState } from "react";
import { formatNumber } from "utils/formats";
import { getResidentCurrentBlockXY, getResidentRewardGraph } from "utils/residentPool";
import "./Rewards.scss";

Chart.register(Filler, TimeSeriesScale, Tooltip);
Chart.defaults.font.family = "Work Sans";

Tooltip.positioners.myCustomPositioner = function (elements, position) {
  if (!elements.length) {
    return false;
  }
  return {
    x: position.x - 15,
    y: position.y - 15
  };
};

const staticTooltip = (event, datasets, symbol) => {
  var chartInstance = event.chart;
  var ctx = chartInstance.ctx;

  datasets.forEach(function (dataset, i) {
    if (i === 0) {
      var meta = chartInstance.getDatasetMeta(i);
      meta.data.forEach(function (bar, index) {
        if (index === 1) {
          var data = dataset.data[index];
          ctx.fillStyle = "#efefef";
          ctx.font = "15px Work Sans";
          ctx.textBaseline = "middle";
          ctx.textAlign = "left";

          const text = `Current Rewards:\n${formatNumber(data.y, 4)} ${symbol}`;
          var lineheight = 15;
          var lines = text.split("\n");
          for (var j = 0; j < lines.length; j++) {
            let x = bar.x - 5 + 120 < event.chart.width ? bar.x - 5 : bar.x - 5 - 120;
            let y = bar.y - 35 + j * lineheight;
            ctx.fillText(lines[j], x, y);
          }
        }
      });
    }
  });
};
const generateChartOptions = (styles, symbol) => {
  const options = {
    type: "line",
    data: {
      datasets: [
        {
          data: [],
          backgroundColor: "#103147",
          borderColor: "#efefef",
          borderDash: [3, 3],
          pointRadius: 5,
          borderWidth: 1
        },
        {
          data: [],
          borderColor: [styles?.base.color],
          borderWidth: 0,
          pointRadius: 2
        },
        {
          data: [],
          backgroundColor: styles ? styles.graph.background : "#fff",
          borderColor: styles ? styles.base.color : "#fff",
          fill: true,
          pointRadius: 0,
          borderWidth: 0.5,
          spanGaps: true
        },
        {
          data: [],
          borderColor: styles ? styles.base.color : "#fff",
          borderWidth: 0.5,
          pointRadius: 0,
          spanGaps: true
        }
      ]
    },
    options: {
      maintainAspectRatio: false,
      responsive: true,
      scales: {
        yAxes: {
          ticks: {
            beginAtZero: true,
            color: "#efefef",
            fontSize: 12,
            precision: 4,
            stepSize: 0.125,
            min: 0,
            callback(value) {
              return value;
            }
          },
          title: {
            display: true,
            color: styles ? styles.base.color : "#fff",
            text: "Tokens / Block",
            font: {
              size: 18
            }
          },
          grid: {
            display: true,
            lineWidth: 0,
            borderWidth: 1.5,
            borderColor: styles ? styles.graph.yaxes : "#fff"
          }
        },
        xAxes: {
          title: {
            display: true,
            text: "Time",
            font: {
              size: 18
            },
            color: styles ? styles.base.color : "#fff"
          },
          type: "timeseries",
          ticks: {
            beginAtZero: true,
            color: "#efefef",
            fontSize: 12,
            maxTicksLimit: 20,
            source: "data"
          },
          time: {
            unit: "day",
            displayFormats: {
              day: "d/MM"
            }
          },
          grid: {
            display: true,
            lineWidth: 0,
            borderWidth: 1.5,
            borderColor: styles ? styles.graph.xaxes : "#fff"
          },
          border: {
            width: 3
          }
        }
      },
      animation: {
        onComplete(event) {
          staticTooltip(event, this.data.datasets, symbol);
        },
        onProgress(event) {
          if (this.data.datasets[0].data.length > 0) {
            staticTooltip(event, this.data.datasets, symbol);
          }
        }
      },
      layout: {
        padding: {
          left: 0,
          right: 0,
          top: 30,
          bottom: 0
        }
      },
      plugins: {
        legend: {
          display: false
        },
        tooltip: {
          displayColors: false,
          bodyFont: {
            family: "Work Sans",
            size: 15
          },
          position: "myCustomPositioner",
          bodyColor: "#efefef",
          backgroundColor: "transparent",
          yAlign: -15,
          xAlign: -15,
          callbacks: {
            title() {},
            beforeLabel() {
              return ``;
            },
            label(tooltipItem) {
              return `${
                tooltipItem.datasetIndex === 2
                  ? ``
                  : `${formatNumber(tooltipItem.formattedValue, 4)} ${symbol}`
              } `;
            }
          }
        }
      }
    }
  };
  return options;
};

function Rewards({ poolIndex, styles, datas, isPoolLoading }) {
  const chart = useRef();
  const Ctx = useRef();
  const [isLoading, setIsLoading] = useState(false);
  const { symbol } = datas;

  const getRewardsTimeSeries = async () => {
    setIsLoading(true);

    const [ts, currentBlockTime] = await Promise.all([
      getResidentRewardGraph(datas, 1000),
      getResidentCurrentBlockXY(datas)
    ]);

    const xVals = ts.map((p) => p.x);

    let beforeBlockPoint = 0;
    let currentBlockTimeUpdated = false;

    ts.forEach((element) => {
      if (element.y < currentBlockTime.y && !currentBlockTimeUpdated) {
        currentBlockTime.x = new Date(element.x - (element.x - beforeBlockPoint) / 2);
        currentBlockTimeUpdated = true;
      }
      beforeBlockPoint = element.x;
    });

    chart.current.data.datasets[0].data = [{ x: currentBlockTime.x, y: 0 }, currentBlockTime];
    chart.current.data.datasets[1].data = ts;
    const backgroundFill = [{ x: ts[0].x, y: 0 }];

    ts.forEach((element) => {
      if (element.x < currentBlockTime.x) {
        backgroundFill.push(element);
      }
    });

    backgroundFill.push(currentBlockTime);
    for (let i = 4; i > 0; i--) {
      backgroundFill.push({
        x: currentBlockTime.x,
        y: (currentBlockTime.y / 5) * i
      });
    }
    chart.current.data.datasets[2].data = backgroundFill;
    chart.current.data.datasets[3].data = ts;
    chart.current.data.labels = xVals;
    if (chart.current.ctx) chart.current.update();
    setIsLoading(false);
  };

  useEffect(() => {
    if (styles && !isPoolLoading) {
      const options = generateChartOptions(styles, symbol);
      chart.current = new Chart(Ctx.current, options);
      getRewardsTimeSeries();
    }

    return () => chart.current?.destroy();
  }, [poolIndex, isPoolLoading, styles]);

  return (
    <div className="graph-container">
      {isLoading && !isPoolLoading ? <Spinner overlay={true} /> : null}
      <canvas ref={Ctx} id="rewardsChart" className="rewardsChart" />
    </div>
  );
}

Rewards.propTypes = {
  name: PropTypes.string
};

export default memo(Rewards);
