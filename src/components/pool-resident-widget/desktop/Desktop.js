import { Button, Col } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { formatNumber } from "utils/formats";

const LeftSide = ({ residentPool, rewardSymbol }) => {
  return (
    <>
      <div>Total Liquidity</div>
      <div>
        <strong>{formatNumber(residentPool.data?.totalLiquidity, 0)} LP</strong>
      </div>
      <div>Current Reward</div>
      <div>
        <strong>
          {formatNumber(residentPool.data?.currentReward, 2)} {rewardSymbol} / Block
        </strong>
      </div>
      <div>
        Pulse Length:
        <strong>
          {` ${formatNumber(residentPool.data?.pulseWavelength, 0)} (~${formatNumber(
            residentPool.data?.pulseLengthByWeeks,
            0
          )}
          ${residentPool.data?.pulseLengthByWeeks > 1.5 ? "weeks" : "week"})`}
        </strong>
      </div>
    </>
  );
};

const RightSide = ({ slots, residentPool, avgAPEPY, avgAPY }) => {
  return (
    <>
      <div>Minimum Deposit:</div>
      <div>
        <strong>{formatNumber(residentPool.data?.minDeposit)} LP</strong>
      </div>
      <div>
        Maximum Deposit: <strong>{formatNumber(residentPool.data?.maxDeposit)} LP</strong>
      </div>
      <div>
        Average APE PY: <strong>{formatNumber(avgAPEPY.data, 0)}%</strong>
      </div>
      <div>
        Average APY: <strong>{formatNumber(avgAPY.data, 0)}%</strong>
      </div>
      <div>
        Filled Lots:
        <strong className="pool-resident-widget__filled-lots">
          {` ${slots.data?.occupied?.length ?? 0}/${residentPool.data?.maxStakers}`}
        </strong>
      </div>
    </>
  );
};

const Footer = ({ image, className, index, styles, tycoon }) => {
  return (
    <>
      <Col xs="6">
        <div className="image-container">
          <img src={image} alt={`${className}-pool`} />
        </div>
      </Col>
      <Col xs="6" className="pool-resident-widget__button">
        <NavLink to={`/pool/resident/${index}`} variant="link">
          <Button
            className={tycoon ? "tycoon-btn" : styles[`${className}-widget-btn`]}
            variant="secondary">
            Pool Details
          </Button>
        </NavLink>
      </Col>
    </>
  );
};

export { LeftSide, RightSide, Footer };
