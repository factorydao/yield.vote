import classnames from "classnames";
import Spinner from "components/indicators/Spinner";
import { MainContext } from "context/context";
import { memo, useContext } from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { useResidentPercentageStats, useResidentSlots } from "utils/queries";
import { useGetPoolGlobals } from "utils/queries/useGetGlobals";
import {
  Footer as DesktopFooter,
  LeftSide as DesktopLeftSide,
  RightSide as DesktopRightSide
} from "./desktop";
import {
  Footer as MobileFooter,
  LeftSide as MobileLeftSide,
  RightSide as MobileRightSide
} from "./mobile";
import "./PoolResidentWidget.scss";

function PoolResidentWidget(props) {
  const { className, styles, index, tycoon } = props;
  const { currentNetwork, isMobile } = useContext(MainContext);
  const prefixClass = "pool-resident-widget";

  const poolIndex = index ?? 1;
  const residentPool = useGetPoolGlobals("resident", poolIndex, currentNetwork);
  const slots = useResidentSlots(poolIndex, currentNetwork);
  const {
    data: { avgAPEPY, avgAPY }
  } = useResidentPercentageStats(slots.data, currentNetwork);
  const poolSymbols = residentPool.data?.symbols ?? {};

  const isLoading =
    residentPool.isFetching ||
    slots.isFetching ||
    avgAPEPY.isFetching ||
    avgAPY.isFetching ||
    !styles;

  const rewardSymbol =
    poolSymbols.reward1TokenSymbol?.[0] !== "$"
      ? `$${poolSymbols.reward1TokenSymbol}`
      : poolSymbols.reward1TokenSymbol;

  const sideProps = { residentPool, slots, avgAPEPY, avgAPY, rewardSymbol, poolIndex };

  const LeftSideComponent = isMobile ? MobileLeftSide : DesktopLeftSide;
  const RightSideComponent = isMobile ? MobileRightSide : DesktopRightSide;
  const FooterComponent = isMobile ? MobileFooter : DesktopFooter;

  return (
    <Col sm="12" md="6" xl="4" className="mb-3 widget">
      <div className={classnames(`${prefixClass}`, styles[`${className}-widget-container`])}>
        {isLoading && <Spinner />}
        {!isLoading && (
          <>
            <div
              className={classnames(
                `${prefixClass}__title`,
                tycoon ? "tycoon-title" : styles[`${className}-widget-title`]
              )}>
              {residentPool.data?.name} Pool
            </div>
            <Row className={`${prefixClass}__data`}>
              <Col xs="6" className={`${prefixClass}__data__left`}>
                <LeftSideComponent {...sideProps} />
              </Col>
              <Col xs="6" className={`${prefixClass}__data__right`}>
                <RightSideComponent {...sideProps} />
              </Col>
            </Row>
            <Row className={`${prefixClass}__footer`}>
              <FooterComponent {...props} />
            </Row>
          </>
        )}
      </div>
    </Col>
  );
}

export default memo(PoolResidentWidget);
