import { MainContext } from "context/context";
import { getAddress } from "ethers/lib/utils";
import { ethInstance } from "evm-chain-scripts";
import jss from "jss";
import PropTypes from "prop-types";
import { memo, useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { generateEventsSubscription } from "utils/eventSub";
import {
  POOL_TYPES,
  TRANSACTION_STATUS,
  approveBasicPoolByType,
  depositBasicByType
} from "utils/poolHelper";
import { use24HrsVolume } from "utils/queries/useDayVolume";
import { useFactoryAccountBalance } from "utils/queries/useFactoryAccountBalance";
import { useGetPoolGlobals } from "utils/queries/useGetGlobals";
import { useGetPositions } from "utils/queries/useGetPositions";
import { usePoolAllowance } from "utils/queries/usePoolAllowance";
import { Desktop as BasicDesktop } from "./desktop";
import { Mobile as BasicMobile } from "./mobile";

const precision = 3;

const getStyle = () => {
  const sheet = jss
    .createStyleSheet({
      "maturity-date-label": { color: "red", fontWeight: 500 }
    })
    .attach();
  return sheet.classes["maturity-date-label"];
};

function PoolBasic({ poolInstance, pageLoading, poolType }) {
  const { address, currentNetwork, isMobile } = useContext(MainContext);
  const [depositAmount, setDepositAmount] = useState("");
  const poolData = useGetPoolGlobals(poolType, poolInstance.value, currentNetwork);
  const accountBalance = useFactoryAccountBalance(poolType, poolInstance.value, currentNetwork);
  const accountAllowance = usePoolAllowance(poolType, poolInstance.value);
  const isFinished = poolData.data?.dates.endTime <= Math.floor(Date.now() / 1e3);
  const contract = poolInstance?.contract;

  const {
    data: { activePositions, inActivePositions },
    isLoading: isAllPositionsLoading,
    refetch: refetchPositions
  } = useGetPositions(poolType, poolInstance.value, currentNetwork, "DepositOccurred");

  const {
    data: withdrawalsEvents,
    isLoading: isWithdrawalsEventsLoading,
    refetch: refetchWithdrawalsEvents
  } = useGetPositions(poolType, poolInstance.value, currentNetwork, "WithdrawalOccurred");

  const isPositionsLoading = isAllPositionsLoading || isWithdrawalsEventsLoading;

  const { data: volume } = use24HrsVolume(poolType, poolInstance.value, currentNetwork, [
    ...Object.values(inActivePositions),
    ...Object.values(activePositions)
  ]);

  const refetchData = async () => {
    refetchPositions();
    refetchWithdrawalsEvents();
  };

  const [showSpinner, setShowSpinner] = useState(false);
  const [tvl, setTvl] = useState(0);
  const [filled, setFilled] = useState(0);
  const [approved, setApproved] = useState(false);
  const [requiredApprove, setRequiredApprove] = useState(false);
  const [pendingTransactions, setPendingTransactions] = useState([]);

  const [showWithdrawDialog, setWithdrawDialog] = useState(false);
  const [withdrawData, setWithdrawData] = useState({});

  const handleChangeTvl = (change) => {
    setTvl(tvl + parseFloat(change));
  };

  const handleCloseWithdrawDialog = () => {
    setWithdrawDialog(false);
  };

  const handleShowWithdrawDialog = (position) => {
    setWithdrawDialog(true);
    const isPending = pendingTransactions.some(
      (x) => x.poolId === poolInstance.value && x.receiptId === position.id
    );
    setWithdrawData({ ...position, isPending: isPending });
  };

  const getPendingTransactions = async () => {
    if (!poolInstance?.value) return;

    const localData = localStorage.getItem("pendingTransactions");
    const pendingTrans = localData ? JSON.parse(localData) : [];
    const poolPendingTransactions = pendingTrans.filter(
      (x) => x.poolId === poolInstance.value && x.poolType === POOL_TYPES.BASIC
    );

    for (let index = 0; index < poolPendingTransactions.length; index++) {
      const element = poolPendingTransactions[index];

      try {
        const transaction = await ethInstance.getTransaction(element.hash);

        if (transaction) {
          const index = poolPendingTransactions.indexOf(element);
          if (index > -1) poolPendingTransactions.splice(index, 1);
        }
      } catch (error) {
        console.error("get transaction:", error);
      }
    }

    localStorage.setItem(
      "pendingTransactions",
      JSON.stringify(poolPendingTransactions.length > 0 ? poolPendingTransactions : [])
    );

    setPendingTransactions(poolPendingTransactions);
  };

  const addNewPendingTransaction = (transaction, positionId) => {
    const newPendingTransactions = [
      ...pendingTransactions,
      {
        poolId: poolInstance.value,
        receiptId: positionId,
        hash: transaction.hash,
        poolType: POOL_TYPES.BASIC
      }
    ];

    setPendingTransactions(newPendingTransactions);

    localStorage.setItem("pendingTransactions", JSON.stringify(newPendingTransactions));
  };

  useEffect(() => {
    getPendingTransactions();
  }, [poolInstance?.value, address, currentNetwork]);

  useEffect(() => {
    if (!contract) return;

    const { address: contractAddress } = contract.networks[currentNetwork];
    if (!contractAddress) return;

    generateEventsSubscription(
      ["WithdrawalOccurred", "DepositOccurred"],
      currentNetwork,
      getAddress(contractAddress),
      refetchData
    );
  }, [contract]);

  var countDecimals = (value) => {
    if (value.includes(".")) {
      return value.split(".")[1].length;
    }
    return 0;
  };

  const precisionLimit = 15;
  const pattern = /^[\d]*\.?[\d]*$/;

  const handleChangeDepositAmount = (data) => {
    const value = data.target.value;
    const precision = countDecimals(value);
    if (precision <= precisionLimit && pattern.test(value)) {
      setDepositAmount(value);
      setRequiredApprove(accountAllowance.data < parseFloat(value));
      setApproved(!(accountAllowance.data < parseFloat(value)));
    } else if (data.target.value === "") {
      setDepositAmount("");
    }
  };

  const approveDeposit = async (depo) => {
    try {
      const setApprove = (result) => {
        setApproved(result.status);
        if (result?.status === TRANSACTION_STATUS.CONFIRMED)
          toast.success(`Transaction approved!`, { position: "top-center" });
        else toast.warn(`Transaction unapproved`, { position: "top-center" });
        setShowSpinner(false);
      };

      setShowSpinner(true);
      const transaction = await approveBasicPoolByType(poolType, depo, poolInstance.value);
      const result = await transaction.wait();

      setApprove(result);
    } catch (error) {
      console.error("claim resident slot failure: " + error);
    }
  };

  const handleDeposit = async (approving = false) => {
    try {
      let depo = depositAmount.toString();
      if (depo !== "") depo = depo.replace(/,/, ".");
      const max = getMaxDepo();
      if (parseFloat(depo) > (max ? parseFloat(max) : 0)) {
        alert(`Deposit can't be higher then max.`);
        return;
      }

      if (approving) {
        await approveDeposit(depo === "" ? "0" : depo);
      } else {
        setShowSpinner(true);
        const transactionDeposit = await depositBasicByType(poolType, poolInstance.value, depo);
        const result = await transactionDeposit.wait();
        if (result.status === TRANSACTION_STATUS.CONFIRMED) {
          setDepositAmount("");
          handleChangeTvl(depo);
          setRequiredApprove(false);
          toast.success(`A deposit has been made!`, { position: "top-center" });
        } else {
          toast.warn(`No deposit has been made.`, { position: "top-center" });
        }
      }
    } catch (error) {
      console.error(`handleDeposit: ` + error.message);
    } finally {
      setShowSpinner(false);
    }
  };

  const getMaxDepo = () => {
    let balance = accountBalance.data ? accountBalance.data : 0;

    if (accountBalance.data?.length > 17) balance = accountBalance.data.slice(0, 17);

    const value = Math.min(balance, poolData.data?.maxPoolSize - poolData.data?.basicTVL);
    return value;
  };

  const setMaxDepositAmount = () => {
    const value = getMaxDepo();
    setDepositAmount(isNaN(value) ? 0 : value);
    setRequiredApprove(accountAllowance.data < parseFloat(value));
    setApproved(!(accountAllowance.data < parseFloat(value)));
  };

  const isStartedPool = poolData.data?.dates.startTime <= Math.floor(Date.now() / 1e3);
  const isPoolActive = isStartedPool && !isFinished;

  const canDeposit =
    isPoolActive && depositAmount > 0 && filled < 100 && accountBalance.data != 0 && address;

  useEffect(() => {
    if (poolData.data) {
      setFilled((tvl * 100) / poolData.data.maxPoolSize);
      setTvl(poolData.data?.basicTVL ? poolData.data.basicTVL : 0);
    }
  }, [tvl, poolData]);

  const BasicComponent = isMobile ? BasicMobile : BasicDesktop;

  const basicComponentProps = {
    poolData,
    pageLoading,
    poolInstance,
    tvl,
    precision,
    filled,
    volume,
    isFinished,
    getStyle,
    depositAmount,
    handleChangeDepositAmount,
    isPoolActive,
    address,
    setMaxDepositAmount,
    requiredApprove,
    canDeposit,
    handleDeposit,
    approved,
    showSpinner,
    poolType,
    activePositions: activePositions ?? {},
    inActivePositions,
    withdrawalsEvents,
    addNewPendingTransaction,
    showWithdrawDialog,
    withdrawData,
    handleCloseWithdrawDialog,
    handleShowWithdrawDialog,
    isPositionsLoading
  };

  return <BasicComponent {...basicComponentProps} />;
}

PoolBasic.propTypes = {
  name: PropTypes.string
};

export default memo(PoolBasic);
