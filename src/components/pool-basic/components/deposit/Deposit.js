import classnames from "classnames";
import Spinner from "components/indicators/Spinner";
import Steps from "components/steps";
import { memo } from "react";
import { InputGroup } from "react-bootstrap";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import styles from "./Deposit.module.scss";

const Deposit = ({
  depositAmount,
  handleChangeDepositAmount,
  isPoolActive,
  address,
  poolInstance,
  setMaxDepositAmount,
  requiredApprove,
  canDeposit,
  handleDeposit,
  approved,
  showSpinner
}) => {
  return (
    <div className={styles.deposit}>
      <div className={styles.title}>Deposit</div>
      <Row>
        <Col sm="8">
          <Row>
            <Col xs="8" md="8" className={styles.inputContainer}>
              <InputGroup className={styles.groupInput}>
                <Form.Control
                  type="text"
                  value={depositAmount}
                  onChange={handleChangeDepositAmount}
                  placeholder="0"
                  isValid={depositAmount > 0}
                  disabled={!isPoolActive || !address}
                />
                <InputGroup.Text>{poolInstance.symbols?.depositTokenSymbol}</InputGroup.Text>
              </InputGroup>
            </Col>
            <Col xs="4" md="4" className={classnames("mt-auto", "mb-1", styles.maxContainer)}>
              <Button
                className={styles.btnOutline}
                variant="outline"
                onClick={() => setMaxDepositAmount()}
                disabled={!isPoolActive || !address}>
                MAX
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>

      {requiredApprove && canDeposit && (
        <div className={styles.depositContainer}>
          <div className={styles.depositLeftContainer}>
            <Steps requiredApprove={requiredApprove} approved={approved} />
          </div>
          <div className={styles.depositRightContainer}>
            <Button
              className={styles.btnSecondary}
              variant="secondary"
              onClick={() => handleDeposit(true)}
              disabled={approved || showSpinner}>
              {approved ? "Approved" : "Approve"}
            </Button>
            <Button
              className={styles.btnSecondary}
              variant="secondary"
              onClick={() => handleDeposit()}
              disabled={!canDeposit || !approved || showSpinner}>
              Deposit
            </Button>
          </div>
        </div>
      )}

      {!requiredApprove && (
        <Button
          className={styles.btnSecondary}
          variant="secondary"
          onClick={() => handleDeposit()}
          disabled={!canDeposit || !approved || showSpinner}>
          Deposit
        </Button>
      )}
      <div>{showSpinner && <Spinner />}</div>
    </div>
  );
};

export default memo(Deposit);
