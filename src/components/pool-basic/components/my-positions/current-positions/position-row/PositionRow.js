import ExpandImage from "assets/expand.svg";
import { MainContext } from "context/context";
import PropTypes from "prop-types";
import { memo, useContext } from "react";
import Button from "react-bootstrap/Button";
import { formatAddress, formatNumber } from "utils/formats";
import "./PositionRow.scss";
const precision = 3;

function PositionRow({
  position,
  withdraw,
  symbols,
  isHolders,
  isPoolFinished,
  isAdditionalRewards
}) {
  const { explorer, isMobile, address } = useContext(MainContext);
  const isConnected = address.length > 0;
  const showWithdrawButton =
    isConnected &&
    (position.owner.toLocaleLowerCase() === address.toLocaleLowerCase() || isPoolFinished);

  return (
    <tr>
      {!isMobile && (
        <td>
          <a href={`${explorer}/tx/` + position.txHash} target="_blank" rel="noreferrer">
            {formatAddress(position.txHash)}
            <img className="expand" src={ExpandImage} alt="Check transaction" />
          </a>
        </td>
      )}
      <td>
        {formatNumber(position.deposit)} {symbols.depositTokenSymbol}
      </td>
      {!isMobile && (
        <td className={isHolders ? "glowie" : ""}>
          {formatNumber(position.currentRewards, precision)} (
          {position.currentYield.toFixed(precision)}%)
        </td>
      )}
      {!isMobile && !isHolders && isAdditionalRewards && (
        <td>
          {formatNumber(position.additionalRewards, precision)} {symbols.reward2TokenSymbol}
        </td>
      )}
      <td className="glowie">
        {formatNumber(position.rewardsAtMaturity, precision)} (
        {formatNumber(position.yieldAtMaturity, precision)}%)
      </td>
      <td>
        {showWithdrawButton && (
          <Button variant="secondary" onClick={() => withdraw(position)}>
            Withdraw
          </Button>
        )}
      </td>
    </tr>
  );
}

PositionRow.propTypes = {
  contractName: PropTypes.string,
  slotId: PropTypes.number
};

export default memo(PositionRow);
