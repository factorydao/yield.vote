import { MainContext } from "context/context";
import PropTypes from "prop-types";
import { useContext, useEffect, useState } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import ClosedPositions from "./closed-positions";
import CurrentPositions from "./current-positions";
import "./MyPositions.scss";

function MyPositions({
  activePositions,
  inActivePositions,
  withdrawalsEvents,
  symbols,
  isFinished,
  handleShowWithdrawDialog
}) {
  const { address, isMobile } = useContext(MainContext);
  const [currentTab, setCurrentTab] = useState(2);

  useEffect(() => {
    setCurrentTab(isFinished ? 1 : 3);
  }, [isFinished]);

  let onlyMyPositions = {};

  Object.values(activePositions).length > 0 && address
    ? Object.keys(activePositions).forEach((key) => {
        if (activePositions[key].owner.toLowerCase() === address.toLowerCase()) {
          onlyMyPositions = { ...onlyMyPositions, [key]: activePositions[key] };
        }
      })
    : {};

  return (
    <Row className="occupied-lots">
      <Col>
        <Row>
          <Col className="occupied-lots__title">
            <Button onClick={() => setCurrentTab(1)} active={currentTab === 1}>
              {`My Open ${!isMobile ? "Positions" : ""} `}
              <strong>{Object.values(onlyMyPositions).length}</strong>
            </Button>
            <Button onClick={() => setCurrentTab(2)} active={currentTab === 2}>
              {`Closed ${!isMobile ? "Positions" : ""} `}
            </Button>
            <Button onClick={() => setCurrentTab(3)} active={currentTab === 3}>
              {`Holders `}
              <strong>{Object.values(activePositions).length}</strong>
            </Button>
          </Col>
        </Row>
        {currentTab === 1 && (
          <CurrentPositions
            handleShowWithdrawDialog={handleShowWithdrawDialog}
            positions={onlyMyPositions}
            symbols={symbols}
            isMobile={isMobile}
            isPoolFinished={isFinished}
          />
        )}
        {currentTab === 2 && (
          <ClosedPositions
            inActivePositions={inActivePositions}
            withdrawalsEvents={withdrawalsEvents}
            symbols={symbols}
            isMobile={isMobile}
          />
        )}
        {currentTab === 3 && (
          <CurrentPositions
            handleShowWithdrawDialog={handleShowWithdrawDialog}
            positions={activePositions}
            symbols={symbols}
            isMobile={isMobile}
            isHolders={true}
            isPoolFinished={isFinished}
          />
        )}
      </Col>
    </Row>
  );
}

MyPositions.propTypes = {
  contractAddress: PropTypes.string
};

export default MyPositions;
