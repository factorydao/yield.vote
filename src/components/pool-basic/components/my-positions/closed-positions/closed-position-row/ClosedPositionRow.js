import ExpandImage from "assets/expand.svg";
import { MainContext } from "context/context";
import PropTypes from "prop-types";
import { memo, useContext } from "react";
import { formatAddress, formatNumber } from "utils/formats";
const precision = 3;

function ClosedPositionRow({ position, withdrawal, symbols }) {
  const { explorer } = useContext(MainContext);

  return (
    <tr>
      <td>
        <a href={`${explorer}/tx/` + position.txHash} target="_blank" rel="noreferrer">
          {formatAddress(position.txHash)}
          <img className="expand" src={ExpandImage} alt="Check transaction" />
        </a>
      </td>
      <td>
        {formatNumber(position.deposit)} {symbols.depositTokenSymbol}
      </td>
      <td className="glowie">
        {formatNumber(position.rewardsAtMaturity, precision)} (
        {formatNumber(position.yieldAtMaturity, precision)}%)
      </td>
      <td>
        <a href={`${explorer}/tx/` + withdrawal} target="_blank" rel="noreferrer">
          {formatAddress(withdrawal)}
          <img className="expand" src={ExpandImage} alt="Check transaction" />
        </a>
      </td>
    </tr>
  );
}

ClosedPositionRow.propTypes = {
  contractName: PropTypes.string,
  slotId: PropTypes.number
};

export default memo(ClosedPositionRow);
