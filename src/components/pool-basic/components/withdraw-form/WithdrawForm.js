import classnames from "classnames";
import CloseModalButton from "components/close-modal";
import Spinner from "components/indicators/Spinner";
import Value from "components/value";
import { MainContext } from "context/context";
import PropTypes from "prop-types";
import { memo, useContext, useState } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import { toast } from "react-toastify";
import { formatNumber } from "utils/formats";
import { TRANSACTION_STATUS, withdrawBasicPoolByType } from "utils/poolHelper";
import "./WithdrawForm.scss";
function WithdrawForm({
  value,
  poolType,
  position,
  closeModal,
  symbols,
  addNewPendingTransaction
}) {
  const [showSpinner, setShowSpinner] = useState(false);
  const { isMobile } = useContext(MainContext);

  const handleClick = async () => {
    try {
      setShowSpinner(true);
      const transaction = await withdrawBasicPoolByType(poolType, position.id, value);
      addNewPendingTransaction(transaction, position.id);

      const result = await transaction.wait();

      if (result?.status === TRANSACTION_STATUS.CONFIRMED)
        toast.success(`Successful payout`, { position: "top-center" });
      else toast.warn(`Fail payout`, { position: "top-center" });

      closeModal(position.id);
    } catch (error) {
      closeModal();
      console.error("WithdrawForm: " + error);
      toast.warn(`Fail payout`, { position: "top-center" });
    } finally {
      setShowSpinner(false);
    }
  };

  return (
    <Container className="position-withdraw" fluid>
      <div className="position-withdraw-header">
        <div className="position-withdraw-header__title">Withdraw Position</div>
        {!isMobile && <CloseModalButton onClick={closeModal} />}
      </div>
      <Form>
        <Row>
          <Col md="6">
            <div className="position-withdraw__label">Deposit</div>
            <Value className="position-withdraw__value-unit" unit={symbols.depositTokenSymbol}>
              {formatNumber(position.deposit)}
            </Value>
          </Col>
          <Col>
            <div className="position-withdraw__label">Current Yield</div>
            <Value
              className={classnames("position-withdraw__value-unit, position-withdraw__green")}
              unit={symbols.reward1TokenSymbol}>
              {formatNumber(position.currentRewards, 3) +
                " (" +
                formatNumber(position.currentYield, 3) +
                "%)"}
            </Value>
          </Col>
        </Row>
        <Row>
          <Col md="6">
            <div className="position-withdraw__label">
              Yield at maturity if you keep your position
            </div>
            <Value className="position-withdraw__value-unit" unit={symbols.reward1TokenSymbol}>
              {formatNumber(position.rewardsAtMaturity, 3) +
                " (" +
                formatNumber(position.yieldAtMaturity, 3) +
                "%)"}
            </Value>
          </Col>
          {position.additionalRewards ? (
            <Col
              style={{
                display: "flex",
                justifyContent: "flex-end",
                flexDirection: "column"
              }}>
              <div className="position-withdraw__label">Additional rewards</div>
              <Value className="position-withdraw__value-unit" unit={symbols.reward2TokenSymbol}>
                {formatNumber(position.additionalRewards, 3)}
              </Value>
            </Col>
          ) : null}
        </Row>
        <Button
          variant="secondary"
          onClick={handleClick}
          disabled={showSpinner || position.isPending}>
          Withdraw
        </Button>
        {position.isPending && (
          <div className="position-withdraw__pending-text">{`Transaction is pending...`}</div>
        )}
        {showSpinner && <Spinner />}
      </Form>
    </Container>
  );
}

WithdrawForm.propTypes = {
  contractAddress: PropTypes.string,
  position: PropTypes.object
};

export default memo(WithdrawForm);
