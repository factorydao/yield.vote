import ExpandImage from "assets/expand.svg";
import Table from "components/table";
import { MainContext } from "context/context";
import { toBN, toWei } from "evm-chain-scripts";
import PropTypes from "prop-types";
import { memo, useContext, useMemo } from "react";
import Button from "react-bootstrap/Button";
import { formatAddress, formatNumber } from "utils/formats";

const link = (cellContent, explorer, isMobile) => {
  return (
    <a href={`${explorer}/address/` + cellContent} target="_blank" rel="noreferrer">
      {formatAddress(cellContent)}
      {!isMobile && <img className="expand" src={ExpandImage} alt="Check transaction" />}
    </a>
  );
};

function LotRows({ slots, claimSlot, withdrawSlot, symbols, isMyLots, isMobile }) {
  const { explorer, address } = useContext(MainContext);

  const rewardSymbol1 =
    (symbols.reward1TokenSymbol?.[0] !== "$" ? ` $` : " ") + symbols.reward1TokenSymbol;
  const rewardSymbol2 =
    (symbols.reward2TokenSymbol?.[0] !== "$" ? ` $` : " ") + symbols.reward2TokenSymbol;

  const isReward2Visible = slots.length > 0 && slots.every((x) => toBN(toWei(x.rewards2)) > 0);

  const buttons = (row, extraData) => {
    return (
      <>
        <Button variant="secondary" disabled={!extraData} onClick={() => claimSlot(row.index)}>
          Claim Lot
        </Button>
        {row.owner.toLowerCase() === extraData?.toLowerCase() && (
          <Button variant="secondary-outline" onClick={() => withdrawSlot(row.index)}>
            Withdraw
          </Button>
        )}
      </>
    );
  };

  const rewardsSum = (cellContent, extraData) => {
    const symbol = extraData
      ? (extraData.reward1TokenSymbol?.[0] !== "$" ? ` $` : " ") + extraData.reward1TokenSymbol
      : "";

    return `${formatNumber(cellContent, 4) + symbol}`;
  };

  const mySlotRewards = (cellContent, row, extraData) => {
    const symbol1 = extraData
      ? (extraData.reward1TokenSymbol?.[0] !== "$" ? ` $` : " ") + extraData?.reward1TokenSymbol
      : "";

    const symbol2 = extraData
      ? (extraData.reward2TokenSymbol?.[0] !== "$" ? ` $` : " ") + extraData.reward2TokenSymbol
      : "";

    return `${
      formatNumber(cellContent, 4) +
      symbol1 +
      (isReward2Visible ? " + " + formatNumber(row.rewards2, 4) + symbol2 : "")
    }`;
  };

  const columnsTable = useMemo(() => [
    {
      accessor: "index",
      Header: "Lot No ⇅"
    },
    {
      accessor: "owner",
      Header: "Occupant ⇅",
      Cell: ({ value }) => link(value, explorer, isMobile)
    },
    {
      accessor: "deposit",
      Header: "Deposit ⇅",
      Cell: ({ value }) => `${formatNumber(value, 4)} SLP`,
      isVisible: !isMobile
    },
    {
      accessor: "burnRatePerPulse",
      Header: "Burn Rate ⇅",
      Cell: ({ value }) => `${formatNumber(value, 4)} SLP / Pulse`,
      isVisible: !isMobile
    },
    {
      accessor: "rewardsSum",
      Header: `Rewards (${rewardSymbol1}) ⇅`,
      Cell: ({ value }) => rewardsSum(value, !isMobile ? symbols : null),
      isVisible: !isMyLots
    },
    {
      accessor: "rewards1",
      Header: `Rewards available (${
        rewardSymbol1 + (isReward2Visible ? "+" + rewardSymbol2 : "")
      }) ⇅`,
      Cell: (props) => mySlotRewards(props.value, props.row.values, !isMobile ? symbols : null),
      isVisible: isMyLots
    },
    {
      accessor: "apepy",
      Header: "APEPY ⇅",
      Cell: ({ value }) => `${value == "∞" ? "∞" : formatNumber(value, 0)} %`,
      isVisible: !isMobile
    },
    {
      accessor: "apy",
      Header: "APY ⇅",
      Cell: ({ value }) => `${value == "∞" ? "∞" : formatNumber(value, 0)} %`,
      isVisible: !isMobile
    },
    {
      accessor: "buttons",
      Header: "",
      Cell: ({ row }) => buttons(row.values, address),
      isVisible: !isMobile
    }
  ]);
  return (
    <div className="occupied-lots__table-container">
      <Table data={slots || []} columns={columnsTable} />
    </div>
  );
}

LotRows.propTypes = {
  userAccount: PropTypes.string,
  slots: PropTypes.array
};

export default memo(LotRows);
