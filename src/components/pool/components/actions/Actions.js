import { memo } from "react";
import { Button } from "react-bootstrap";

const Actions = ({
  isFuturePool,
  address,
  handleShowStakeDialog,
  handleShowWithdrawDialog,
  slotIds,
  residentSlots
}) => {
  return (
    <>
      <Button
        variant="secondary"
        disabled={isFuturePool || !address}
        onClick={() => handleShowStakeDialog()}>
        Stake LP Tokens
      </Button>
      <Button
        variant="outline-secondary"
        onClick={() => handleShowWithdrawDialog()}
        disabled={
          (slotIds ? slotIds.my?.length === 0 : true) || residentSlots.isLoading || !address
        }>
        Withdraw
      </Button>
    </>
  );
};

export default memo(Actions);
