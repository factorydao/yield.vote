import Back from "components/back";
import ResidentStakeLPForm from "components/resident-stake-lp-form";
import ResidentWithdrawForm from "components/resident-withdraw-form";
import PoolName from "../../pool-basic/components/pool-name";
import Actions from "../components/actions/Actions";
import PoolDetails from "../components/details";
import Graph from "../components/graph";
import HeaderStats from "../components/header-stats";
import OccupiedLots from "../components/occupied-lots/";
import SummaryStats from "../components/summary-stats";
import mobileStyles from "./Mobile.module.scss";

const Mobile = ({
  residentPool,
  avgAPEPY,
  avgAPY,
  isFuturePool,
  address,
  handleShowStakeDialog,
  handleShowWithdrawDialog,
  slotIds,
  residentSlots,
  volume,
  mySlots,
  myRewards,
  reward1Symbol,
  poolIndex,
  dataForGraph,
  partnerStyles,
  poolSymbols,
  showStakeDialog,
  handleCloseStakeDialog,
  claimSlot,
  approveSlot,
  userLPBalance,
  selectedLot,
  approving,
  accountAllowance,
  showImages,
  showWithdrawDialog,
  handleCloseWithdrawDialog,
  slotAction,
  lastAccountTransactions,
  setLastAccountTransactions
}) => {
  const handleBackOnClick = () => {
    handleCloseWithdrawDialog();
    handleCloseStakeDialog();
  };

  return (
    <div className={mobileStyles.container}>
      {(showStakeDialog || showWithdrawDialog) && <Back onClick={() => handleBackOnClick()} />}

      {!showStakeDialog && !showWithdrawDialog && (
        <>
          <PoolName className={mobileStyles.poolName} poolName={residentPool.data?.name} />
          <HeaderStats
            className={mobileStyles.headerStats}
            avgAPEPY={avgAPEPY}
            avgAPY={avgAPY}
            residentPool={residentPool}
          />
          <div className={mobileStyles.summaryContainer}>
            <SummaryStats
              myRewards={myRewards}
              mySlots={mySlots}
              reward1Symbol={reward1Symbol}
              isMobile={true}
            />
          </div>
          <div className={mobileStyles.actionsContainer}>
            <Actions
              isFuturePool={isFuturePool}
              address={address}
              handleShowStakeDialog={handleShowStakeDialog}
              handleShowWithdrawDialog={handleShowWithdrawDialog}
              slotIds={slotIds}
              residentSlots={residentSlots}
            />
          </div>
          <div className={mobileStyles.detailsContainer}>
            <PoolDetails volume={volume} residentPool={residentPool} slotIds={slotIds} />
          </div>
          <div className={mobileStyles.graphContainer}>
            <Graph
              poolIndex={poolIndex}
              dataForGraph={dataForGraph}
              partnerStyles={partnerStyles}
              residentPool={residentPool}
              reward1Symbol={reward1Symbol}
            />
          </div>
          <div className={mobileStyles.lotsContainer}>
            <OccupiedLots
              mySlots={mySlots}
              occupied={slotIds?.occupied}
              claimSlot={handleShowStakeDialog}
              withdrawSlot={handleShowWithdrawDialog}
              symbols={poolSymbols}
              isMobile={true}
            />
          </div>
        </>
      )}

      {showStakeDialog && (
        <ResidentStakeLPForm
          tokenAddress={residentPool.data?.liquidityToken}
          claimSlot={claimSlot}
          approve={approveSlot}
          closeModal={handleCloseStakeDialog}
          poolIndex={poolIndex ? parseInt(poolIndex) : null}
          lpBalance={userLPBalance ? parseFloat(userLPBalance.data).toFixed(2) : "0"}
          slot={selectedLot}
          slotIds={slotIds}
          residentPool={residentPool}
          approving={approving}
          accountAllowance={accountAllowance.data ?? 0}
          showImages={showImages}
          symbols={poolSymbols}
        />
      )}

      {showWithdrawDialog && (
        <ResidentWithdrawForm
          slotAction={slotAction}
          poolIndex={poolIndex ? parseInt(poolIndex) : null}
          closeModal={handleCloseWithdrawDialog}
          slot={selectedLot}
          slotIds={mySlots}
          symbols={poolSymbols}
          showImages={showImages}
          lastAccountTransactions={lastAccountTransactions}
          setLastAccountTransactions={setLastAccountTransactions}
        />
      )}
    </div>
  );
};

export { Mobile };
