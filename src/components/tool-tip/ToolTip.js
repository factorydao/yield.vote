import { memo } from "react";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";
import styles from "./ToolTip.module.scss";

const ToolTip = ({ text }) => {
  return (
    <OverlayTrigger placement="right" overlay={<Tooltip>{text}</Tooltip>}>
      <div className={styles.questionMark}>?</div>
    </OverlayTrigger>
  );
};

export default memo(ToolTip);
