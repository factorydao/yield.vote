import { memo, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import Select from "react-select";
import styles from "./Menu.module.scss";

const customStyles = {
  option: (styles, { data, isSelected, isDisabled }) => ({
    ...styles,
    fontSize: 16,
    fontWeight: 400,
    borderLeft: isSelected ? `3px solid ${data.borderColor}` : "3px solid transparent",
    backgroundColor: isSelected ? data.background : "transparent",
    ":active": {
      color: data.color,
      borderLeft: !isDisabled && `3px solid ${data.borderColor}`,
      backgroundColor: isSelected ? data.background : "transparent"
    },
    ":hover": {
      ...styles[":active"],
      color: !isDisabled && data.color,
      backgroundColor: !isDisabled && data.background,
      borderLeft: !isDisabled && `3px solid ${data.borderColor}`,
      cursor: !isDisabled ? "pointer" : "not-allowed"
    }
  }),
  menu: (styles) => ({
    ...styles,
    borderRadius: 0,
    marginTop: 0,
    border: "1px solid var(--dark-green-blue)"
  }),
  menuList: (styles) => ({
    ...styles,
    backgroundColor: "var(--dark-slate-blue)"
  }),
  control: (styles) => ({
    ...styles,
    fontSize: 16,
    fontWeight: 400,
    borderRadius: 0,
    border: "1px solid var(--dark-green-blue)",
    backgroundColor: "var(--dark-slate-blue)",
    cursor: "pointer"
  }),
  placeholder: (styles) => ({ ...styles, fontSize: 16, color: "#fff" }),
  singleValue: (styles, { data }) => ({ ...styles, fontSize: 16, color: data.color }),
  groupHeading: (styles, { data }) => ({
    ...styles,
    marginLeft: 3,
    color: data.options[0]?.color
  })
};

const Menu = ({ options }) => {
  const history = useNavigate();
  const location = useLocation();
  const allOptions = Array.prototype.concat.apply(
    [],
    options.map((partner) => partner.options)
  );

  const [selectedOption, setSelectedOption] = useState(
    allOptions.find((option) => option.url === location.pathname)
  );

  const handleOptionChange = (option) => {
    setSelectedOption(option);
    history.push({ pathname: option.url });
  };

  return (
    <div className={styles.container}>
      <Select
        placeholder="&#9776; Menu"
        isSearchable={false}
        styles={customStyles}
        options={options}
        defaultValue={selectedOption}
        components={{
          IndicatorSeparator: () => null
        }}
        onChange={handleOptionChange}
        controlShouldRenderValue={false}
      />
    </div>
  );
};

export default memo(Menu);
