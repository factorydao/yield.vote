import classnames from "classnames";
import Spinner from "components/indicators/Spinner";
import { MainContext } from "context/context";
import { memo, useContext } from "react";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { useGetPoolGlobals } from "utils/queries/useGetGlobals";
import { usePoolSymbols } from "utils/queries/usePoolSymbols";
import "./PoolBasicWidget.scss";
import {
  Footer as DesktopFooter,
  LeftSide as DesktopLeftSide,
  RightSide as DesktopRightSide
} from "./desktop";
import {
  Footer as MobileFooter,
  LeftSide as MobileLeftSide,
  RightSide as MobileRightSide
} from "./mobile";

function PoolBasicWidget(props) {
  const { className, styles, poolIndex, contractType } = props;
  const { currentNetwork, isMobile } = useContext(MainContext);

  const prefixClass = "pool-basic-widget";

  const pool = useGetPoolGlobals(contractType, poolIndex, currentNetwork);
  const poolSymbols = usePoolSymbols(contractType, poolIndex, currentNetwork);

  const isLoading = !styles || poolSymbols.isLoading || pool.isLoading;

  const symbol =
    poolSymbols.data?.depositTokenSymbol?.[0] !== "$"
      ? `$${poolSymbols.data?.depositTokenSymbol}`
      : poolSymbols.data?.depositTokenSymbol;

  const sideProps = { pool, symbol };

  let LeftSideComponent = isMobile ? MobileLeftSide : DesktopLeftSide;
  let RightSideComponent = isMobile ? MobileRightSide : DesktopRightSide;
  let FooterComponent = isMobile ? MobileFooter : DesktopFooter;

  return (
    <Col sm="12" md="6" xl="4" className="mb-3 widget">
      <div className={classnames(`${prefixClass}`, styles[`${className}-widget-container`])}>
        {isLoading && pool.data ? (
          <Spinner />
        ) : (
          <>
            <div
              className={classnames(`${prefixClass}__title`, styles[`${className}-widget-title`])}>
              {pool.data?.name}
            </div>
            <Row className={`${prefixClass}__data`}>
              <Col xs="6" className={`${prefixClass}__data__left`}>
                <LeftSideComponent {...sideProps} />
              </Col>
              <Col xs="6" className={`${prefixClass}__data__right`}>
                <RightSideComponent {...sideProps} />
              </Col>
            </Row>
            <Row className={`${prefixClass}__footer`}>
              <FooterComponent {...props} />
            </Row>
          </>
        )}
      </div>
    </Col>
  );
}

export default memo(PoolBasicWidget);
