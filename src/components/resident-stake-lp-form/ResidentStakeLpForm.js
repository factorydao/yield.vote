import HouseImage from "assets/house-01.svg";
import * as houses from "assets/houses";
import { MainContext } from "context/context";
import { ethInstance, fromWei } from "evm-chain-scripts";
import PropTypes from "prop-types";
import { memo, useContext, useEffect, useMemo, useState } from "react";
import { toast } from "react-toastify";
import { formatNumber } from "utils/formats";
import { TRANSACTION_STATUS } from "utils/poolHelper";
import { useFactoryAccountBalance } from "utils/queries/useFactoryAccountBalance";
import {
  getResidentAPEPYFromBurnRate,
  getResidentBurnPerPulse,
  getResidentClaimMinimums
} from "utils/residentPool";
import { useResidentWavelength } from "../../utils/queries";
import { Desktop as DesktopComponent } from "./desktop";
import { Mobile as MobileComponent } from "./mobile";

const precisionLimit = 15;

function ResidentStakeLpForm({
  poolIndex,
  residentPool,
  closeModal,
  slot,
  slotIds,
  claimSlot,
  approve,
  approving,
  accountAllowance,
  showImages,
  symbols
}) {
  const { networkData, address, isMobile, currentNetwork } = useContext(MainContext);

  const [showSpinner, setShowSpinner] = useState(true);
  const [claimInProgress, setClaimInProgress] = useState(false);
  const [selectedSlot, setSelectedSlot] = useState(null);
  const [selectedSlotImage, setSelectedSlotImage] = useState(HouseImage);
  const [defaultSelectedSlot, setDefaultSelectedSlot] = useState(null);
  const [requiredApprove, setRequiredApprove] = useState(false);
  const [approved, setApproved] = useState(false);
  const lpBalance = useFactoryAccountBalance(
    "resident",
    residentPool.data?.liquidityToken,
    address
  );

  const slotSelectableOptions = useMemo(
    () =>
      slotIds.all
        ? slotIds.all.map((slot, i) => ({
            value: i,
            label: `Lot ${slot.index} - ` + (slot.vacant ? "vacant" : "occupied")
          }))
        : [],
    [slotIds]
  );

  const [burnRate, setBurnRate] = useState("");
  const [deposit, setDeposit] = useState("");
  const [poolShare, setPoolShare] = useState("");
  const [newAPEPY, setNewAPEPY] = useState("∞");
  const wavelength = useResidentWavelength(poolIndex, currentNetwork);
  const [errors, setErrors] = useState({
    depositError: null,
    burnRateError: null
  });

  const clearForm = () => {
    setDeposit("");
    setBurnRate("");
    setNewAPEPY("∞");
    setErrors({ depositError: null, burnRateError: null });
    setRequiredApprove(false);
  };

  const handleSlot = async (approving = false) => {
    let burn = 0;
    let depo = 0;
    if (deposit !== "") depo = parseFloat(deposit.replace(/,/, "."));

    const enoughLP = parseFloat(lpBalance?.data) >= depo;
    if (!enoughLP) {
      alert("Not enough LP in your wallet.");
      return;
    }

    const withinBoundsDepo =
      depo >= parseFloat(residentPool.data?.minDeposit) &&
      depo <= parseFloat(residentPool.data?.maxDeposit);

    if (!withinBoundsDepo) {
      alert("Deposit must be between minimum and maximum for this pool.");
      return;
    }

    if (burnRate !== "") burn = parseFloat(burnRate.replace(/,/, "."));

    const withinBoundsBurn =
      burn >= parseFloat(residentPool.data?.minBurnRatePerPulse) &&
      burn <= parseFloat(residentPool.data?.maxBurnRatePerPulse);

    if (!withinBoundsBurn) {
      alert("Burn rate must be between minimum and maximum for this pool.");
      return;
    }

    let betterDeal = false;

    if (selectedSlot?.vacant) {
      const data = await getResidentClaimMinimums(poolIndex, selectedSlot.index);

      const minDeposit = parseFloat(fromWei(data.minDeposit));
      const minBurnRate = parseFloat(
        fromWei(await getResidentBurnPerPulse(poolIndex, data.minBurnRate))
      );
      betterDeal = burn >= minBurnRate && depo >= minDeposit;
    } else {
      const selectedDeposit = parseFloat(selectedSlot.deposit);
      const depositDeal =
        burn > parseFloat(selectedSlot.burnRatePerPulse) &&
        (depo > parseFloat(selectedDeposit) || depo == parseFloat(residentPool.data?.maxDeposit));
      betterDeal = depositDeal || selectedDeposit == 0;
    }

    if (selectedSlot && betterDeal) {
      try {
        if (approving) {
          await approve(depo, setApproved);
        } else {
          setShowSpinner(true);
          setClaimInProgress(true);
          const result = await claimSlot(poolIndex, selectedSlot.index, burn, depo);

          if (result.status === TRANSACTION_STATUS.CONFIRMED)
            toast.success(`A deposit has been made!`, {
              position: "top-center"
            });
          else toast.warn(`No deposit has been made.`, { position: "top-center" });
        }
      } catch (error) {
        toast.warn(`No deposit has been made.`, { position: "top-center" });
        console.log(error);
      } finally {
        setClaimInProgress(false);
        setShowSpinner(false);
      }
      if (!approving) closeModal();
    } else {
      alert("Must supply a higher deposit and a higher burn rate than current occupant.");
    }
  };

  const validateDepositInput = async (value) => {
    if (value >= 0) {
      let betterDepo = selectedSlot.deposit;

      if (selectedSlot.vacant) {
        const data = await getResidentClaimMinimums(poolIndex, selectedSlot.index);
        betterDepo = fromWei(data.minDeposit);
      }

      const maxDepo = parseFloat(residentPool.data.maxDeposit);
      const minDepo =
        parseFloat(betterDepo) > parseFloat(residentPool.data.minDeposit)
          ? betterDepo
          : residentPool.data.minDeposit;

      let valueLTMin = false;

      if (selectedSlot.vacant) {
        valueLTMin = value < parseFloat(minDepo);
      } else if (parseFloat(selectedSlot.deposit) == 0) {
        valueLTMin = false;
      } else {
        valueLTMin = value <= parseFloat(minDepo);
      }

      if (valueLTMin)
        setErrors({
          ...errors,
          depositError: `Deposit must be ${selectedSlot.vacant ? "min." : ">"} ${minDepo}`
        });
      else if (value > parseFloat(maxDepo))
        setErrors({
          ...errors,
          depositError: `Deposit must be <= ${maxDepo}`
        });
      else setErrors({ ...errors, depositError: null });
    } else if (parseFloat(selectedSlot.deposit) != 0)
      setErrors({ ...errors, depositError: "Deposit is required" });
  };

  const setMaxDepositAmount = async () => {
    let balance = lpBalance?.data ? lpBalance?.data : 0;
    if (lpBalance?.data.length > 17) {
      balance = lpBalance.data.slice(0, 17);
    }

    const minimum = Math.min(balance, residentPool.data?.maxDeposit);
    setDeposit(minimum.toString());

    await validateDepositInput(minimum);
    setRequiredApprove(!(accountAllowance > minimum));
    setApproved(accountAllowance > minimum);
    calcPoolShare(minimum);
  };

  const loadValuesForSlot = async (slotIndex) => {
    clearForm();
    const initSlot = slotIds.all[slotIndex];
    let betterDeposit = initSlot.deposit;
    let betterBurnRate =
      parseFloat(initSlot.deposit) == 0 && !initSlot.vacant
        ? residentPool.data?.minBurnRatePerPulse
        : initSlot.burnRatePerPulse;

    const lastUpdatedTime = !initSlot.vacant
      ? (await ethInstance.getBlock(parseInt(initSlot.lastUpdatedBlock))).timestamp
      : Date.now();

    if (initSlot.vacant) {
      const data = await getResidentClaimMinimums(poolIndex, initSlot.index);

      const minBurnRatePerPulse = await getResidentBurnPerPulse(poolIndex, data.minBurnRate);

      betterDeposit = fromWei(data.minDeposit);
      betterBurnRate = fromWei(minBurnRatePerPulse);
    }

    const minDepo = Math.max(residentPool.data?.minDeposit, betterDeposit);

    const minBurn =
      parseFloat(residentPool.data?.minBurnRatePerPulse) > parseFloat(betterBurnRate)
        ? residentPool.data?.minBurnRatePerPulse
        : betterBurnRate;

    setDefaultSelectedSlot(slotSelectableOptions[slotIndex]);
    setSelectedSlot({
      ...initSlot,
      lastUpdatedTime,
      minDeposit: minDepo,
      minBurnRate: minBurn
    });
  };

  async function handleSlotChange({ value }) {
    await loadValuesForSlot(value);
  }

  var countDecimals = (value) => {
    if (value.includes(".")) {
      return value.split(".")[1].length;
    }
    return 0;
  };

  const calcPoolShare = (deposit) => {
    const totalLiquidity = residentPool.data?.totalLiquidity;
    const poolShare = (deposit / totalLiquidity) * 100;
    let formattedValue = formatNumber(poolShare, 4);
    if (poolShare < 0.0001 && poolShare > 0) formattedValue = "<0.0001";

    setPoolShare(formattedValue);
  };

  const pattern = /^[\d]*\.?[\d]*$/;

  const handleDeposit = async (event) => {
    const value = event.target.value;
    const precision = countDecimals(value);
    calcPoolShare(value);
    if (precision <= precisionLimit && pattern.test(value)) {
      setDeposit(value.toString());
      await validateDepositInput(parseFloat(value));
      setRequiredApprove(accountAllowance < parseFloat(value) || accountAllowance === 0);
      setApproved(!(accountAllowance < parseFloat(value)) && accountAllowance > 0);
    } else if (value == "") {
      setDeposit("");
    }
  };

  const handleDepositOnBlur = async (event) => {
    const value = parseFloat(event.target.value);
    calcPoolShare(isNaN(value) ? "0" : value);
    await validateDepositInput(value);
  };

  const handleBurnRateOnBlur = async (event) => {
    const value = parseFloat(event.target.value);
    if (value >= 0) {
      let betterBurn =
        parseFloat(selectedSlot.deposit) == 0 && !selectedSlot.vacant
          ? residentPool.data?.minBurnRatePerPulse
          : selectedSlot.burnRatePerPulse;

      if (selectedSlot.vacant) {
        const data = await getResidentClaimMinimums(poolIndex, selectedSlot.index);

        const minBurnRatePerPulse = await getResidentBurnPerPulse(poolIndex, data.minBurnRate);

        betterBurn = fromWei(minBurnRatePerPulse);
      }

      const maxBurn = parseFloat(residentPool.data?.maxBurnRatePerPulse);
      const minBurn =
        parseFloat(residentPool.data.minBurnRatePerPulse) > parseFloat(betterBurn)
          ? residentPool.data?.minBurnRatePerPulse
          : betterBurn;

      let valueLTMin = false;

      if (selectedSlot.vacant) {
        valueLTMin = value < parseFloat(minBurn);
      } else if (parseFloat(selectedSlot.deposit) == 0) {
        valueLTMin = false;
      } else {
        valueLTMin = value <= parseFloat(minBurn);
      }

      if (valueLTMin)
        setErrors({
          ...errors,
          burnRateError: `Burn rate must be ${selectedSlot.vacant ? "min." : ">"} ${minBurn}`
        });
      else if (value > maxBurn)
        setErrors({
          ...errors,
          burnRateError: `Burn rate value must be <= ${maxBurn}`
        });
      else setErrors({ ...errors, burnRateError: null });
    } else if (parseFloat(selectedSlot.deposit) != 0)
      setErrors({ ...errors, burnRateError: "Burn rate is required" });
  };

  async function handleBurnRateChange(event) {
    const precision = countDecimals(event.target.value);
    if (precision <= precisionLimit) {
      const value = event.target.value;
      if (pattern.test(value)) {
        setBurnRate(event.target.value);
        const float = parseFloat(event.target.value.replace(/,/, "."));
        if (float) {
          const apepy = await getResidentAPEPYFromBurnRate(poolIndex, event.target.value);

          setNewAPEPY(apepy);
        } else {
          setNewAPEPY("∞");
        }
      } else if (value == "") {
        setBurnRate("");
      }
    }
  }

  function getBlocksTillBurned(depo, burnPerPulse) {
    if (wavelength.data && burnPerPulse > 0) {
      const burnPerBlock = burnPerPulse / wavelength.data.toNumber();
      const blocksTillBurned = depo / burnPerBlock;

      return blocksTillBurned;
    } else {
      return "∞";
    }
  }

  useEffect(() => {
    if (slotSelectableOptions.length && !claimInProgress) {
      setShowSpinner(false);
    }
    (async () => {
      if (slotSelectableOptions.length && !selectedSlot) {
        const slotIndex = slot ? slotIds.all.findIndex((slotData) => slotData.index === slot) : 0;
        await loadValuesForSlot(slotIndex);
      }
    })();
  }, [slotSelectableOptions]);

  useEffect(() => {
    let houseImage = HouseImage;
    const imageName = `Lot${selectedSlot?.index}${selectedSlot?.vacant ? "Vacant" : "Occupied"}`;

    if (houses[imageName]) {
      houseImage = houses[imageName].default;
    }

    setSelectedSlotImage(houseImage);
  }, [selectedSlot]);

  useEffect(() => {
    clearForm();
  }, [address]);

  const stakeComponentProps = {
    lpBalance,
    deposit,
    selectedSlot,
    handleDeposit,
    handleDepositOnBlur,
    errors,
    burnRate,
    handleBurnRateChange,
    handleBurnRateOnBlur,
    newAPEPY,
    approved,
    requiredApprove,
    setMaxDepositAmount,
    handleSlot,
    showSpinner,
    getBlocksTillBurned,
    closeModal,
    slotSelectableOptions,
    handleSlotChange,
    defaultSelectedSlot,
    showImages,
    selectedSlotImage,
    networkData,
    approving,
    poolShare,
    symbols
  };

  const StakeComponent = isMobile ? MobileComponent : DesktopComponent;

  return (
    <>
      <StakeComponent {...stakeComponentProps} />
    </>
  );
}

ResidentStakeLpForm.propTypes = {
  poolIndex: PropTypes.number,
  position: PropTypes.object
};

export default memo(ResidentStakeLpForm);
