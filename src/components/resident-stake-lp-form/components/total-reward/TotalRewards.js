import { memo } from "react";
import { formatNumber } from "utils/formats";

const TotalComponent = ({ rewards, symbol }) => {
  const rewardSymbol = (symbol?.[0] !== "$" ? ` $` : " ") + `${symbol}`;
  return (
    <>
      <div>
        Total Rewards: {formatNumber(rewards, 4)} {rewardSymbol}
      </div>
    </>
  );
};

export default memo(TotalComponent);
