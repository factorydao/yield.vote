import classnames from "classnames";
import Steps from "components/steps";
import ToolTip from "components/tool-tip";
import Value from "components/value";
import { MainContext } from "context/context";
import { memo, useContext } from "react";
import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import { formatNumber } from "utils/formats";
import buttonStyles from "../../buttons.module.scss";
import Label from "../label";
import OccupiedTime from "../occupied-time";
import TillBurned from "../till-burned";
import TotalRewards from "../total-reward";
import styles from "./Occupy.module.scss";

const Occupy = ({
  lpBalance,
  deposit,
  selectedSlot,
  handleDeposit,
  handleDepositOnBlur,
  errors,
  burnRate,
  handleBurnRateChange,
  handleBurnRateOnBlur,
  newAPEPY,
  approved,
  requiredApprove,
  setMaxDepositAmount,
  handleSlot,
  showSpinner,
  getBlocksTillBurned,
  symbols
}) => {
  const { isMobile } = useContext(MainContext);
  const showApproveButton = requiredApprove && !errors.depositError && !errors.burnRateError;

  return (
    <div className={styles.container}>
      <div className={styles.occupiedLeft}>
        {showApproveButton && <Steps requiredApprove={requiredApprove} approved={approved} />}
      </div>
      <div className={styles.occupiedCenter}>
        <div className={styles.tokenBalance}>
          SLP Token Balance: <span>{formatNumber(lpBalance?.data)} SLP</span>
        </div>
        <Label>New Deposit (LP)</Label>
        <Row>
          <Col xs="9">
            <Form.Group>
              <Form.Control
                className={classnames(styles.value, styles.highlighted)}
                min={0}
                value={deposit}
                placeholder={`Min. ${
                  selectedSlot?.deposit < selectedSlot.minDeposit || selectedSlot?.deposit == 0
                    ? ""
                    : ">"
                } ${selectedSlot.minDeposit}`}
                onChange={handleDeposit}
                onBlur={handleDepositOnBlur}
                isInvalid={!!errors.depositError}
                inputMode="numeric"
              />
              <Form.Control.Feedback type="invalid">{errors.depositError}</Form.Control.Feedback>
            </Form.Group>
          </Col>
          <Col xs="2">
            <Button
              className={buttonStyles.btnOutline}
              variant="outline"
              onClick={setMaxDepositAmount}>
              MAX
            </Button>
          </Col>
        </Row>
        <Label>New Burn Rate (LP / Pulse)</Label>
        <Form.Group>
          <Form.Control
            className={classnames(styles.value, styles.formControl)}
            min={0}
            value={burnRate}
            placeholder={`Min. ${
              selectedSlot.burnRatePerPulse < selectedSlot.minBurnRate || selectedSlot?.deposit == 0
                ? ""
                : ">"
            } ${selectedSlot.minBurnRate}`}
            onChange={handleBurnRateChange}
            onBlur={handleBurnRateOnBlur}
            isInvalid={!!errors.burnRateError}
            inputMode="numeric"
          />
          <Form.Control.Feedback type="invalid">{errors.burnRateError}</Form.Control.Feedback>
        </Form.Group>

        <Label>
          <span>
            New APEPY <ToolTip text="APEPY = rewardsPerPulse / burnPerPulse" />
          </span>
        </Label>
        <Value className={styles.value} unit="%">
          {newAPEPY ? formatNumber(newAPEPY) : 0}
        </Value>

        {isMobile && (
          <div className={styles.footerStats}>
            <TillBurned getBlocksTillBurned={getBlocksTillBurned} selectedSlot={selectedSlot} />
            <TotalRewards rewards={selectedSlot.rewardsSum} symbol={symbols?.reward1TokenSymbol} />
            <OccupiedTime lastUpdatedTimestamp={selectedSlot.lastUpdatedTime} />
          </div>
        )}

        {showApproveButton && (
          <Button
            className={buttonStyles.btnSecondary}
            variant="secondary"
            onClick={() => handleSlot(true)}
            disabled={approved}>
            {approved ? "Approved" : "Approve"}
          </Button>
        )}
        <Button
          className={buttonStyles.btnSecondary}
          variant="secondary"
          onClick={() => handleSlot()}
          disabled={
            showSpinner || !selectedSlot || errors.burnRateError || errors.depositError || !approved
          }>
          Occupy Lot
        </Button>
      </div>
      <div className={styles.occupiedRight}></div>
    </div>
  );
};

export default memo(Occupy);
