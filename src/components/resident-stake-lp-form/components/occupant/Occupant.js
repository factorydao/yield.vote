import ExpandImage from "assets/expand.svg";
import { MainContext } from "context/context";
import { memo, useContext } from "react";
import { formatAddress } from "utils/formats";
import Label from "../label";

const Occupant = ({ selectedSlot }) => {
  const { explorer } = useContext(MainContext);

  return (
    <Label>
      <a
        href={
          `${explorer} 
          +/address/` + selectedSlot?.owner
        }
        target="_blank"
        rel="noreferrer">
        Occupant: {selectedSlot ? formatAddress(selectedSlot.owner) : ""}
        <img className="expand" src={ExpandImage} alt="Check address" />
      </a>
    </Label>
  );
};

export default memo(Occupant);
