import { memo } from "react";
import styles from "./Label.module.scss";
import classnames from "classnames";

const Label = ({ className, children }) => {
  return <div className={classnames(className, styles.label)}>{children}</div>;
};

export default memo(Label);
