import classnames from "classnames";
import Spinner from "components/indicators/Spinner";
import React from "react";
import ReactSelect from "react-select";
import Claim from "../components/claim";
import Occupy from "../components/occupy";
import mobileStyles from "./Mobile.module.scss";

const Mobile = (props) => {
  const {
    selectedSlot,
    showSpinner,
    slotSelectableOptions,
    handleSlotChange,
    defaultSelectedSlot,
    showImages,
    selectedSlotImage,
    approving
  } = props;

  return (
    <div className={mobileStyles.container}>
      <div className={classnames(mobileStyles.lotsContainer, mobileStyles.card)}>
        <ReactSelect
          className="select"
          classNamePrefix="select"
          options={slotSelectableOptions}
          onChange={handleSlotChange}
          value={defaultSelectedSlot}
        />
        <div>Select a vacant lot or choose an occupied lot to kick out the owner</div>
        <div className={classnames("mt-5", "mb-4", "text-center")}>
          {showImages && <img className="expand" src={selectedSlotImage} alt="It's a house" />}
        </div>
      </div>

      {selectedSlot?.vacant && (
        <div className={classnames(mobileStyles.claimContainer, mobileStyles.card)}>
          <div className={mobileStyles.title}>Lot details</div>
          <Claim {...props} />
        </div>
      )}

      {selectedSlot && !selectedSlot?.vacant && (
        <div className={classnames(mobileStyles.occupyContainer)}>
          <Occupy {...props} />
        </div>
      )}
      {(showSpinner || approving) && <Spinner overlay={true} />}
    </div>
  );
};

export { Mobile };
