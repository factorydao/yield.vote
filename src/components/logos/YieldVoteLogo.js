import Image from "./YieldVoteLogo.svg";

function YieldVoteLogo() {
  return <img src={Image} alt="yield.vote" />;
}

export default YieldVoteLogo;
