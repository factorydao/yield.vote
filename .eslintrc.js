module.exports = {
  root: true, // Make sure eslint picks up the config at the root of the directory
  parserOptions: {
    ecmaVersion: 2020, // Use the latest ecmascript standard
    sourceType: "module" // Allows using import/export statements
  },
  settings: {
    react: {
      version: "detect" // Automatically detect the react version
    }
  },
  env: {
    browser: true, // Enables browser globals like window and document
    amd: true, // Enables require() and define() as global variables as per the amd spec.
    node: true, // Enables Node.js global variables and Node.js scoping.
    es6: true
  },
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:prettier/recommended" // Make this the last element so prettier config overrides other formatting rules
  ],
  rules: {
    "no-debugger": "warn",
    "react/prop-types": "off",
    "prettier/prettier": ["error", { usePrettierrc: false }], // Use our .prettierrc file as source
    "react/react-in-jsx-scope": "off",
    "no-nested-ternary": "error",
    "no-unused-vars": "warn"
  }
};
